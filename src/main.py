from display import app, routes, web_socket, msg

import threading
import argparse
import os

if __name__ == "__main__":
	parser = argparse.ArgumentParser()
	parser.add_argument("-d", dest="debug", type=bool)
	args = parser.parse_args()

	print("Running as user", os.getuid())

	if args.debug:
		web_socket.run(app, debug=True)
	else:
		web_socket.run(app, host="0.0.0.0")
