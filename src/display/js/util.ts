class Util {
    static get_unique(array: Array<any>): Array<any> {
        let unique = [];
        for (let item of array) {
            if ($.inArray(item, unique) === -1) {
                unique.push(item);
            }
        }
        return unique;
    }

    static arrays_equal(array1: Array<any>, array2: Array<any>): boolean {
        if (array1.length !== array2.length) {
            return false;
        }

        for (let i = 0; i < array1.length; i++) {
            if (array1[i] !== array2[i]) {
                return false;
            }
        }

        return true;
    }
}
