/* globals vis, initial_state, app, visualisation*/

/* globals
	initial_layout
	add_node
	add_edge
*/

class Visualisation {
    container: any;
    edges: any; // vis.DataSet
    nodes: any; // vis.DataSet
    states: Array<any>;  /// List of visualisation states
    network: any;

    constructor() {
        console.log("Constructing visualisation");
        this.container = document.getElementById("visualisation");
        this.edges = new vis.DataSet();
        this.nodes = new vis.DataSet();
        this.states = [];
        this.network = null;

        console.log("Done constructing visualisation");
    }

    init(): void {
        let data = {
            nodes: this.nodes,
            edges: this.edges
        };

        let options = {
            interaction: {
                multiselect: true
            }
        };

        this.network = new vis.Network(this.container, data, options);

        this.network.on("afterDrawing", this.draw_node_decoration);
    }

    draw_node_decoration(context) {
        let node_ids = visualisation.nodes.get().map((node) => { return node.id; });
        let nodes_positions = visualisation.network.getPositions(node_ids);
        context.fillStyle = "#FF0000";

        for (let node_id in nodes_positions) {
            let node = visualisation.nodes.get(node_id);
            if (node.load !== undefined) {
                node_pos = nodes_positions[node_id];
                context.fillRect(node_pos.x + 10, node_pos.y, 5, - node.load * 20);
            }
        }
    }

	/**
	 * Get the current background from the store, and the the background-image property of the canvas
	 */
    set_background(): void {
        let background_name = app.get_current_state().background;

        if (background_name === undefined) {
            console.warn("background_name is undefined for state, setting it to default.png");
            app.edit_current_state("background", "default.png");
            background_name = "default.png";
        }

        let background_url = "/api/background/" + background_name;

        console.log("Setting background to:", background_url);
        $("#visualisation").css("background-image", "url(" + background_url + ")");
    }

	/**
	 * Resets the entire visualisation to make way for a new layout
	 */
    clear(): void {
        this.nodes.clear();
        this.edges.clear();
        app.state.layout = initial_layout();
        app.state.current_layout = "New Layout";

        this.load_current_state();
        this.set_background();
    }

	/**
	 * Load the selected state from the currently loaded layout
	 */
    load_current_state(): void {
        let index = app.state.current_state_index;
        let state = app.state.layout.states[index];

        if (state === undefined) {
            throw ("Current state is undefined");
        }

        this.load_state(state);
    }

    load_state(state): void {
        if (typeof state.edges === "undefined") {
            throw ("state.edges is undefined");
        }

        if (typeof state.nodes === "undefined") {
            throw ("state.node is undefined");
        }

        console.log("Loading:", state);
        this.nodes.clear();
        this.edges.clear();

        // add nodes
        for (let node of state.nodes) {
            add_node(node);
        }

        // add edges
        for (let edge of state.edges) {
            add_edge(edge);
        }

        // load background
        this.set_background();
    }

    /**
     * Checks if a router is added, if not adds it to the router list
     * @param  {string} router_id id of the router to add
     */
    add_new_router(router_id: string) {
        let router_ids = app.state.routers.map((router) => { return router.id; });
        let pos = $.inArray(router_id, router_ids);

        if (pos === -1) { // if not found
            let routers = app.state.routers;
            routers.push({ id: router_id });
            routers.sort();
            app.setState({ "routers": routers });
        }
    }
}

// Sometimes I think JavaScript ignores my comments :/
