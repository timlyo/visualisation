/* globals visualisation, Interface, app */
/// <reference path="interface.ts"/>

/**
 * Update the edges with layout information
 */
function update_edges(data) {
    visualisation.edges.update(data.edges);
}

/**
 * Add a route between two nodes
 * @param {string} from_node 	ID of the node to start the route at
 * @param {string} to_node	 	ID of the node to end the route at
 * @param {string} bearer	 		The edge's bearer
 */
function create_edge(from_node, to_node, bearer = null) {
    visualisation.edges.add({
        from: from_node,
        to: to_node,
        bearer: bearer,
    });
}

function add_edge(edge) {
    visualisation.edges.add(edge);
}

/**
 * Add edges between the selected nodes
 */
function link_selected(): void {
    let nodes = visualisation.network.getSelectedNodes();
    if (nodes.length === 0) {
        return;
    }

    // link all to first
    let first_node = nodes[0];
    for (let node of nodes) {
        if (node === first_node) {
            continue;
        }
        create_edge(first_node, node);
    }
}


function setup_edge_events(visualisation) {
    visualisation.network.on("selectEdge", function(properties) {
        Interface.open_edge_settings();
        app.setState({
            "selected_edge_ids": visualisation.network.getSelectedEdges()
        });
    });

    visualisation.network.on("deselectEdge", function(properties) {
        Interface.close_edge_settings();
        app.setState({
            "selected_edge_ids": visualisation.network.getSelectedEdges()
        });
    });
}

function reset_all_edges_to_default_style(): void {
    for (let edge of visualisation.edges.get()) {
        reset_edge_to_default_style(edge);
    }
}

function reset_edge_to_default_style(edge) {
    edge.dashes = false;
    edge.color = "#2B7CE9";
}

/**
 * Sets each edge's style to fit in with the bearer's style
 * Must be called after edge's change bearer or bearers change their style
 */
function apply_bearer_style(): void {
    for (let edge of visualisation.edges.get()) {
        reset_edge_to_default_style(edge);
        if (typeof edge.bearer === "undefined") {
            continue;
        }

        let bearer_name = edge.bearer;
        let style = app.state.layout.bearers.filter((bearer) => bearer.name === bearer_name)[0].style;
        $.extend(edge, style);

        visualisation.edges.update(edge);
    }
}

function set_selected_edges_bearer(new_bearer) {
    for (let edge of visualisation.network.getSelectedEdges()) {
        visualisation.edges.update({
            id: edge,
            bearer: new_bearer
        });
    }
}
