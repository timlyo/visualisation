/* globals visualisation, app */

let $ = $;

class Data {
	/**
	 * Update the list of backgrounds from the server
	 * @return {[type]} [description]
	 */
    static update_backgrounds(): void {
        console.log("Updating background list");
        $.ajax({
            url: "/api/background",
            success: function(result) {
                app.setState({ "backgrounds": result.data });
            }
        });
    }

	/**
	 * Update the list of icons from the server
	 */
    static update_icons(): void {
        console.log("Updating icon list");
        $.ajax({
            url: "/api/icon",
            success: function(result) {
                app.setState({ "icons": result.data });
            }
        });
    }

	/**
	 * Load the file list from the display and send an action to the store
	 */
    static update_file_list(): void {
        console.log("Updating file list");
        $.ajax({
            url: "/api/load",
            success: function(result) {
                if (result.data === undefined) {
                    console.error("/api/load/ returned undefined for result.data");
                }

                console.log("Got file list:", result.data);
                app.setState({ "files": result.data });
            }
        });
    }

	/**
	 * Load the file list from the server and save the result in the state
	 */
    static load_layout(name) {
        console.log("Loading layout");
        $.ajax({
            url: "/api/load/" + name,

            success: function(result) {
                if (result.data === undefined) {
                    console.error("/api/load/", name, "returned undefined for result.data");
                    $.notify("Failed to load " + name, "error");
                }

                console.log("Loaded layout", name, result.data);
                app.setState({ "layout": result.data });

                visualisation.load_current_state();
            }
        });
    }

    static delete_layout(name) {
        console.log("Deleting layout ", name);
        $.ajax({
            url: "/api/load/" + name,
            method: "DELETE",

            success: function(result) {
                console.log(result);
                $.notify(name + " has been deleted", "success");
                Data.update_file_list();
            }
        });
    }

	/**
	 * Formats and sends the layout to the server so that it can be saved permenantly
	 */
    static post_layout_to_server() {
        app.save_visualisation_state();

        let name = app.state.layout.name;
        let url = "/api/save/" + name;
        console.log("Posting layout to", url);
        console.log("Layout:", app.state.layout);

        $.ajax({
            url: url,
            contentType: "application/json",
            data: JSON.stringify(app.state.layout),
            method: "POST",

            success: function() {
                $.notify("Layout saved", "success");
            },

            error: function(jqXHR, textStatus, errorThrown) {
                $.notify("Layout failed to save\n" + textStatus + "\n" + errorThrown, "error");
                console.error("Layout failed to save\n" + textStatus + "\n" + errorThrown);
            },
        });

        Data.update_file_list();
    }
}
