let Cookies = window.Cookies;

/**
 * Create any cookies that don't already exist
 */
function init_cookies(): void {
    if (Cookies.get("edit_mode") == null) {
        Cookies.set("edit_mode", true);
    }
}
