/*globals Data, visualisation */
/// <reference path="interface.ts"/>
/* globals
  init_cookies
  setup_node_events
  setup_edge_events
*/

init_cookies();
$(".ui.dropdown").dropdown();
$(".popup").popup();

Interface.init_draggables();
Data.update_backgrounds();
Data.update_icons();

visualisation.init();
visualisation.load_current_state();
setup_node_events(visualisation);
setup_edge_events(visualisation);
