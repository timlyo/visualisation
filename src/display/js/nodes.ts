/* globals vis, visualisation, types, app */
/// <reference path="interface.ts"/>

let visualisation = visualisation;

function add_node(node) {
    node = complete_node(node);
    try {
        // console.log("adding node:", node);
        visualisation.nodes.add(node);
    } catch (e) {
        console.error(e);
    }
}

/**
 * Adds anything to a node that should be in every node
 * @param  {object} node Node to be completed
 * @return {object}      completed node
 */
function complete_node(node) {
    node.font = {
        strokeColor: "#DDDDDD",
        strokeWidth: 1
    };

    return node;
}

/**
 * update the nodes with layout information
 * @param  {Array} data [New node information]
 */
function update_nodes(data) {
    visualisation.nodes.update(data.nodes);
}

/**
 * Construct a new empty node, and add it to the visualisation
 */
function new_node() {
    add_node({
        label: visualisation.nodes.length.toString(),
        x: Math.random() * 400 - 200, // random position so nodes aren't created
        y: Math.random() * 400 - 200  // on top of each other
    });
}


/**
 * Handles the initialisation for nodes
 * Sets up the actions for user interaction
 */
function setup_node_events(visualisation): void {
    console.assert(visualisation.network !== undefined);

    // Allow nodes to be moved whilst being dragged
    visualisation.network.on("dragStart", function(properties) {
        for (let node of properties.nodes) {
            visualisation.nodes.update({
                id: node,
                fixed: false
            });
        }
    });

    // Freeze nodes that have been dragged
    visualisation.network.on("dragEnd", function(properties) {
        for (let node of properties.nodes) {
            visualisation.nodes.update({
                id: node,
                fixed: true,
            });
        }

        // this is keep the NodeSettingsWindow's fixed tick box up to date
        if (app.state.node_settings_open) {
            app.forceUpdate();
        }
    });

    visualisation.network.on("selectNode", function(properties) {
        Interface.open_node_settings();
        console.log("nodes Selected");
        app.setState({
            "selected_node_ids": visualisation.network.getSelectedNodes()
        });
    });

    visualisation.network.on("deselectNode", function(properties) {
        Interface.close_node_settings();
        app.setState({
            "selected_node_ids": visualisation.network.getSelectedNodes()
        });
    });
}

/**
 * Set the image for all selected nodes
 * @param {string} image_src url of the image to be displayed
 */
function set_selected_nodes_image(image_src: string): void {
    let select_nodes: Array<string> = visualisation.network.getSelectedNodes();

    console.log("Changing selected nodes icon's to:", image_src);

    for (let node of select_nodes) {
        visualisation.nodes.update({
            id: node,
            shape: "image",
            image: image_src
        });
    }
}

function set_selected_nodes_physics(value): void {
    for (let node of visualisation.network.getSelectedNodes()) {
        console.log(node, value);
        visualisation.nodes.update({
            id: node,
            fixed: value
        });
    }
    console.log(visualisation.nodes);
}

function set_selected_nodes_size(new_size): void {
    for (let node of visualisation.network.getSelectedNodes()) {
        visualisation.nodes.update({
            id: node,
            size: new_size
        });
    }
}

function delete_selected_nodes(): void {
    for (let node of visualisation.network.getSelectedNodes()) {
        visualisation.nodes.remove(node);
    }

    app.setState({ "selected_node_ids": visualisation.network.getSelectedNodes() });
}

function set_selected_nodes_label(label): void {
    for (let node of visualisation.network.getSelectedNodes()) {
        visualisation.nodes.update({
            id: node,
            label: label,
        });
    }
}

/**
 * Sets whether the nodes can move or not
 * @param {Boolean} is_fixed true if the node cannot move
 */
function set_selected_nodes_fixed(is_fixed): void {
    for (let node of visualisation.network.getSelectedNodes()) {
        visualisation.nodes.update({
            id: node,
            fixed: is_fixed
        });
    }
}

function set_selected_nodes_router(router_id: string): void {
    console.assert(typeof router_id === "string");
    console.log(router_id);
    for (let node of visualisation.network.getSelectedNodes()) {
        visualisation.nodes.update({
            id: node,
            router_id: router_id
        });
    }
}

/**
 * Apply changes recived from server to nodes
 * Sets a property on the node object representing the data that is collected by the controller
 * @param {object} changes object containing the changes
 */
function apply_node_changes(changes: object): void {
    for (let node of visualisation.nodes.get()) {
        if (changes.router_id !== node.router_id) {
            continue;
        }

        if (changes.load !== undefined) {
            visualisation.nodes.update({
                id: node.id,
                load: changes.load
            });
        }
    }
}
