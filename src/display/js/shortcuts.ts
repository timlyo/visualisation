/// <reference path="interface.ts"/>
/* globals
	link_selected
	new_node
	delete_selected_nodes
*/

let Mousetrap = window.Mousetrap;

console.log("Binding shortcuts");

Mousetrap.bind("ctrl+s", function(): boolean {
    Interface.open_save();
    return false;
});

Mousetrap.bind("ctrl+o", function(): boolean {
    Interface.open_open();
    return false;
});

Mousetrap.bind("esc", function(): boolean {
    Interface.close_all();
    return false;
});

Mousetrap.bind("l", function(): boolean {
    if (Interface.is_visualisation_focussed()) {
        link_selected();
        return false;
    }
});

Mousetrap.bind("a", function(): boolean {
    if (Interface.is_visualisation_focussed()) {
        new_node();
        return false;
    }
});

Mousetrap.bind("del", function(): boolean {
    if (Interface.is_visualisation_focussed()) {
        delete_selected_nodes();
        return false;
    }
});

Mousetrap.bind("up", function(): void {
    if (app.set_state_index(app.state.current_state_index - 1)) {
        visualisation.load_current_state();
    }
});

Mousetrap.bind("down", function(): void {
    if (app.set_state_index(app.state.current_state_index + 1)) {
        visualisation.load_current_state();
    }
});

Mousetrap.bind("ctrl+m", function(): void {
    app.append_new_state();
});

Mousetrap.bind("ctrl+\\", function(): void {
    Interface.invert_sidebar();
});

Mousetrap.prototype.stopCallback = function(): boolean {
    return false;
};
