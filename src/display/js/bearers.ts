/* globals app */

interface Bearer {
    name: string;
    style: Style;
}

interface Style {
    dashes: boolean;
    color: string;
    width: number;
}

function default_bearer(): Bearer {
    let bearer = {
        name: "Bearer",
        style: complete_bearer_style({})
    };

    return bearer;
}

/**
 * Return the bearer_name with a postfix to make it unique
 * @param {string}        bearer_name  The bearers name
 * @param {Array<string>} bearer_names Array of currently existing bearer names
 */
function uniqueify_bearer_name(bearer_name: string, bearer_names: Array<string>): string {
    // must be unique if only bearer
    if (bearer_names.length === 0) {
        return bearer_name;
    }

    let suffix = "";

    // while name is in the array
    // Done to generate a unique name
    // Worst case of O(n)
    while ($.inArray(bearer_name + suffix, bearer_names) !== -1) {
        if (suffix === "") {
            suffix = 1;
        } else {
            suffix += 1;
        }
    }

    if (suffix !== "") {
        return bearer_name + suffix;
    }

    return bearer_name;
}

function delete_bearer(name: string): void {
    let new_bearers = app.state.layout.bearers.filter(bearer => bearer.name !== name);
    app.modify_layout({ "bearers": new_bearers });
}

/**
 * Fills in uncompleted styles with defaults
 * @param  {object} style Style to be completed
 * @return {object}       completed style
 */
function complete_bearer_style(old_style: Style): Style {
    let style: Style = $.extend({}, old_style);

    if (style.dashes === undefined) {
        style.dashes = false;
    }

    if (style.color === undefined) {
        style.color = "#000";
    }

    if (style.width === undefined) {
        style.width = 1.0;
    }

    return style;
}
