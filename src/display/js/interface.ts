/* globals interact, visualisation, Data, types, Interface, app */
/* globals
	Cookies
*/

let app = app;

/**
 * This class is a container for interface functions
 * Many of them are wrappers around store events. This is done to keep the callbacks succinct
 */
class Interface {
    static is_visualisation_focussed(): boolean {
        if (document.activeElement === document.body) {
            return true;
        }
        return false; // returns false
    }

	/**
	 * Initialise the draggable windows
	 */
    static init_draggables(): void {
        interact(".draggable")
            .draggable({
                onmove: Interface.dragParentListener,
                restrict: {
                    restriction: "parent",
                }
            });
    }

	/**
	 * Initialise the dropzones for uploading icons and backgrounds
	 */
    static init_dropzone(): void {
        console.log("Creating dropzone");
        let dropzone = $(".dropzone").dropzone({
            init: function() {
                this.on("success", function(file) {
                    console.log("File uploaded");
                    Data.update_backgrounds();
                    Data.update_icons();
                });
            }
        });
    }

    static dragParentListener(event): void {
        // assumes that the draggable is the first child of the element that should be moved
        let target = $(event.target).parent().get(0);

        let x = (parseFloat(target.getAttribute("data-x")) || 0) + event.dx;
        let y = (parseFloat(target.getAttribute("data-y")) || 0) + event.dy;

        target.style.webkitTransform =
            target.style.transform = "translate(" + x + "px, " + y + "px)";

        target.setAttribute("data-x", x);
        target.setAttribute("data-y", y);
    }

    static open_open(): void {
        app.setState({ "open_open": true });
    }

    static close_open(): void {
        app.setState({ "open_open": false });
    }

    static open_save(): void {
        app.setState({ "save_open": true });
    }

    static close_save(): void {
        app.setState({ "save_open": false });
    }

    static open_backgrounds(): void {
        app.setState({ "background_open": true });
    }

    static close_backgrounds(): void {
        app.setState({ "background_open": false });
    }

    static open_icons(): void {
        app.setState({ "icons_open": true });
    }

    static close_icons(): void {
        app.setState({ "icons_open": false });
    }

    static open_bearer_editor() {
        app.setState({ "bearer_editor_open": true });
    }

    static close_bearer_editor() {
        app.setState({ "bearer_editor_open": false });
    }

    static open_bearer_window() {
        app.setState({ "bearers_open": true });
    }

    static close_bearer_window() {
        app.setState({ "bearers_open": false });
    }

    static toggle_bearer_window() {
        if (app.state.bearers_open) {
            app.setState({ "bearers_open": false });
        } else {
            app.setState({ "bearers_open": true });
        }
    }

    static open_settings(): void {
        alert("WIP");
    }

    static close_settings(): void {
        alert("WIP");
    }

    static open_node_settings(): void {
        if (app.state.edit_mode) {
            app.setState({ "node_settings_open": true });
        }
    }

    static close_node_settings(): void {
        app.setState({ "node_settings_open": false });
        Interface.reset_html_cursor();
    }

    static open_edge_settings(): void {
        if (app.state.edit_mode) {
            app.setState({ "edge_settings_open": true });
        }
    }

    static close_edge_settings(): void {
        app.setState({ "edge_settings_open": false });
        Interface.reset_html_cursor();
    }

    static open_sidebar(): void {
        app.setState({ sidebar_open: true });
    }

    static close_sidebar(): void {
        app.setState({ sidebar_open: false });
    }

    static invert_sidebar() {
        if (app.state.sidebar_open) {
            Interface.close_sidebar();
        } else {
            Interface.open_sidebar();
        }
        return false;
    }

    static enter_edit_mode(): void {
        app.setState({ "edit_mode": true });
        Cookies.set("edit_mode", true);
        if (visualisation.nodes.get().length > 0) {
            Interface.open_node_settings();
        }

        if (visualisation.edges.get().length > 0) {
            Interface.open_edge_settings();
        }
    }

    static exit_edit_mode(): void {
        app.setState({ "edit_mode": false });
        Cookies.set("edit_mode", false);
        Interface.close_edge_settings();
        Interface.close_node_settings();
    }

	/**
	 * This function is used to remove the `"cursor: move;"` from the `<html>` tag that is incorrectly retained after a movable window is closed
	 */
    static reset_html_cursor(): void {
        $("html").css("cursor", "auto");
    }

    static close_all(): void {
        // console.log("Closing");
        let state = app.state;

        // only close background windows if there's no foreground windows
        if (state.open_open || state.save_open || state.background_open || state.icons_open || state.bearer_editor_open) {
            Interface.close_open();
            Interface.close_save();
            Interface.close_backgrounds();
            Interface.close_icons();
            Interface.close_bearer_editor();
        } else {
            Interface.close_node_settings();
            Interface.close_edge_settings();
        }
    }
}
