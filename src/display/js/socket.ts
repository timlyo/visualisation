/**
 * Web socket connection between the client and the server
 */

let socket = io.connect("http://" + document.domain + ":" + location.port);

socket.on("connect", function() {
    console.log("Connected socket");
});

socket.on("changes", function(changes) {
    let router_id = changes.id;
    changes = JSON.parse(changes.changes); // normalise to contained structure
    console.log("change to", router_id, ":", changes);
    visualisation.add_new_router(router_id);

    changes.router_id = router_id;

    apply_node_changes(changes);
});
