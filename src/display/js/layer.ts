/* globals app, visualisation */

/**
 * Contains methods to apply and unapply layers
 * Should be called whenever that layer is active, and information relevant
 * to that layer has changed
 */

class Layer {
	/**
	 * Apply the connected layer
	 * If an edge.connected = true then display edge
	 */
    static apply_connected_layer(): void {
        app.state.layers_applied.connected = true;
        for (let edge of visualisation.edges.get()) {

            // not connected is checked first so that link is displayed
            // if the property is missing
            if (!edge.connected) {
                edge.hidden = true;
            } else {
                edge.hidden = false;
            }
            visualisation.edges.update(edge);
        }
    }

	/**
	 * Remove any attributes added by the connected layer
	 */
    static reset_connected_layer(): void {
        app.state.layers_applied.connected = false;
        for (let edge of visualisation.edges.get()) {
            edge.hidden = false;
            visualisation.edges.update(edge);
        }
    }

    static apply_queue_length_layer(): void {

    }
}
