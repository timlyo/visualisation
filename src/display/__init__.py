from flask import Flask
from flask_socketio import SocketIO

from display import data

app = Flask(__name__)
app.config["SECRET_KEY"] = "Super Duper Secret"  # TODO Something sensible
web_socket = SocketIO(app)

from display import routes
