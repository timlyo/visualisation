import json
import eventlet
import sys
from eventlet.green import socket

from flask_socketio import emit

from display import web_socket

"""
Connection between the controller and the display
"""

def listen():
    controller_socket = None

    while True:
        try:
            if controller_socket is None:
                controller_socket = create_socket()

            request_updates(controller_socket)

        except EOFError as e:
            print(e)
            controller_socket.close()
            controller_socket = None

        web_socket.sleep(0.5)


def create_socket() -> socket:
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    address = "127.0.0.1"
    while True:
        try:
            s.connect((address, 5001))
            break
        except socket.error as e:
            web_socket.sleep(1)
            continue
    print("Connected to socket")
    return s


def request_updates(connection: socket) -> str:
    send_message(connection, {"action": "get_updates"})
    updates = get_message(connection)

    for update in updates:
        web_socket.emit("changes", update)


def send_message(connection, message):
    # print("Sending: ", message)
    if isinstance(message, dict):
        message = json.dumps(message)

    if isinstance(message, str):
        message = message.encode()

    connection.sendall(message)


def get_message(connection: socket) -> str:
    """Gets a message from the connected socket"""
    size = connection.recv(5)
    if len(size) == 0:
        print("Connection closed")
        raise EOFError("Socket closed")

    data = connection.recv(int(size))
    try:
        data = json.loads(data.decode())
    except json.JSONDecodeError as e:
        print(e)
        data = data.decode()

    return data


def handle_message(message):
    data = json.loads(message)
    print(data)
    web_socket.emit("controller", "Controller message", broadcast=True)
    print("Relayed")
