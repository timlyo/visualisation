# handles loading files from disk
import os
import json
from datetime import datetime

from PIL import Image

data_dir = os.path.expanduser('/var/visualisation')
save_dir = os.path.join(data_dir, "saves")
delete_path = os.path.join(save_dir, "deleted")
backgrounds_dir = os.path.join(data_dir, "backgrounds")
icons_dir = os.path.join(data_dir, "icons")

if not os.path.exists(save_dir): os.makedirs(save_dir)
if not os.path.exists(delete_path): os.makedirs(delete_path)
if not os.path.exists(backgrounds_dir): os.makedirs(backgrounds_dir)
if not os.path.exists(icons_dir): os.makedirs(icons_dir)


def load_save(name: str) -> dict:
    with open(os.join(save_dir, name)) as file:
        return json.load(file)


def get_save_list() -> list:
    files = os.scandir(save_dir)
    info = []
    # A list of details to extract from the file
    details = ("name", "date", "description")
    # TODO send over count of edges and nodes
    for fp in files:
        if fp.is_dir():
            continue

        with open(fp.path) as file:
            data = json.load(file)
            # filter unnecessary details
            data = {k: v for (k, v) in data.items() if k in details}
            info.append(data)
    return info


def save_layout(layout: dict):
    if "name" not in layout:
        print("Layout has no name")
        raise KeyError("Layout has no name")
    name = layout["name"]

    if not name.endswith(".json"):
        name = name + ".json"

    layout["date"] = datetime.now().strftime("%x")

    file_path = os.path.join(save_dir, name)
    with open(file_path, 'w') as out_file:
        json.dump(layout, out_file)


def delete_layout(name: str):
    if not name.endswith(".json"):
        name += ".json"

    file_path = os.path.join(save_dir, name)
    new_path = os.path.join(delete_path, name)

    if not os.path.exists(delete_path):
        os.mkdir(delete_path)

    if os.path.isfile(file_path):
        os.replace(file_path, new_path)
    else:
        raise FileNotFoundError(name + " does not exist")


def get_file_by_name(name: str) -> dict:
    files = os.scandir(save_dir)
    for file_name in files:
        if file_name.is_dir():
            continue

        with open(file_name.path) as file:
            data = json.load(file)
            if data["name"] == name or file_name == name:
                return data
            else:
                continue


def get_background_list() -> list:
    files = ["/api/background/" + x for x in os.listdir(backgrounds_dir)]
    return files


def save_background(file):
    path = os.path.join(backgrounds_dir, file.filename)
    if os.path.isfile(path):
        raise ValueError("{} already exists".format(file.filename))
    file.save(path)


def get_icon_list() -> list:
    files = ["/api/icon/" + x for x in os.listdir(icons_dir)]
    return files


def save_icon(file):
    path = os.path.join(icons_dir, file.filename)
    if os.path.isfile(path):
        raise ValueError("{} already exists".format(file.filename))

    size = (200, 200)

    print(path)
    with Image.open(file) as image:
        image.thumbnail(size, Image.ANTIALIAS)

        image.save(path)
