/* globals uniqueify_bearer_name, Test */

(function test_uniqueify_bearer_name() {
    console.log("[TEST] testing test_uniqueify_bearer_name");
    let bearers = [];
    const name = "bearer";

    bearers.push(uniqueify_bearer_name(name, bearers));
    Test.expect(bearers[0], "bearer");

    bearers.push(uniqueify_bearer_name(name, bearers));
    Test.expect(bearers[1], "bearer1");

    bearers.push(uniqueify_bearer_name(name, bearers));
    Test.expect(bearers[2], "bearer2");

    console.log("[TEST✔] test_uniqueify_bearer_name ran successfully");
})();

function test_delete_bearers() {

}
