/* globals complete_node, Test */

(function test_complete_node(): void {
    console.log("[TEST] testing complete_bearer_style");
    let empty_node = {};

    empty_node = complete_node(empty_node);
    Test.is_defined(empty_node.font);

    console.log("[TEST✔] complete_bearer_style ran successfully");
})();
