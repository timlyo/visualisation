/* globals Test */
/* globals
  get_node_label_denominator
*/

(function test_node_label_denominators(): void {
    console.log("[TEST] testing test_node_label_denominators");
    let nodes: Array<any> = [{
        label: "one"
    }];

    // one node
    Test.expect(get_node_label_denominator(nodes), "one");

    nodes.push({
        label: "one"
    });

    // two nodes same name
    Test.expect(get_node_label_denominator(nodes), "one");

    nodes.push({
        label: "two"
    });

    // three nodes different name
    Test.expect(get_node_label_denominator(nodes), "");
    console.log("[TEST✔] test_node_label_denominators ran successfully");
})();
