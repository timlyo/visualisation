/* globals app, Test */
/* globals initial_layout */

(function test_modify_layout(): void {
    console.log("[TEST] testing test_layout");
    Test.expect(app.state.layout.name, "New Layout");
    Test.expect(app.state.layout.bearers.length, initial_layout().bearers.length);
    Test.expect(app.state.layout.states.length, 1);

    app.modify_layout({
        "name": "test_name"
    });

    Test.expect(app.state.layout.name, "test_name");
    Test.expect(app.state.layout.bearers.length, initial_layout().bearers.length);

    app.setState({
        "layout": initial_layout()
    });

    Test.expect(app.state.layout.name, "New Layout");
    Test.expect(app.state.layout.bearers.length, initial_layout().bearers.length);
    Test.expect(app.state.layout.states.length, 1);
    console.log("[TEST✔] test_layout ran successfully");
})();
