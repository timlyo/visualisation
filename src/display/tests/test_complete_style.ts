/* globals complete_bearer_style, Test */

(function test_completeStyle(): void {
    console.log("[TEST] testing complete_bearer_style");
    let style = {};
    style = complete_bearer_style(style);

    Test.is_defined(style.dashes);
    Test.expect(style.dashes, false);
    Test.is_defined(style.color);
    Test.expect(style.color, "#000");
    Test.is_defined(style.width);
    Test.expect(style.width, 1.0);

    let dashes = {
        dashes: true
    };

    dashes = complete_bearer_style(dashes);
    Test.is_defined(dashes.dashes);
    Test.expect(dashes.dashes, true);

    let coloured = {
        color: "#FFF"
    };

    coloured = complete_bearer_style(coloured);
    Test.is_defined(coloured.color);
    Test.expect(coloured.color, "#FFF");

    let width = {
        width: 2.0
    };

    width = complete_bearer_style(width);
    Test.is_defined(width.width);
    Test.expect(width.width, 2.0);
    console.log("[TEST✔] complete_bearer_style ran successfully");
})();
