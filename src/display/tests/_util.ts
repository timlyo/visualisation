// prepended with an underscore so that it appears first in the directory
// and is prepended at start of the tests.js file

/* globals Util*/

class Test {
    static expect(value, expected_value): void {
        if (value !== expected_value) {
            debugger;
            throw ("Expected " + expected_value + " got " + value);
        }
    }

    static is_different_object(object1, object2) {
        if (object1 === object2) {
            throw (object1 + " is the same object as " + object2);
        }
    }

    static does_throw_error(callable) {
        try {
            callable();
        } catch (err) {
            return;
        }
        debugger;
        throw ("function did not throw an error");
    }

    static test_DOM_element_is_null(element_id): void {
        let DOM_element = document.getElementById(element_id);
        if (DOM_element === null) {
            return;
        } else {
            debugger;
            throw ("test_DOM_element_is_null failed for " + element_id + "=" + DOM_element);
        }
    }

    static test_DOM_element_not_null(element_id): void {
        let DOM_element = document.getElementById(element_id);
        if (DOM_element !== null) {
            return;
        } else {
            debugger;
            throw ("test_DOM_element_not_null failed for " + element_id + "=" + DOM_element);
        }
    }

    static test_DOM_element_has_children(element_id, minimum = 1): void {
        let children = $("#" + element_id).children().toArray();
        if (children.length >= minimum) {
            return;
        } else {
            debugger;
            throw ("test_DOM_element_has_children failed " + element_id + " has " + children.length + " children. (minimum=" + minimum + ")");
        }
    }

    static test_DOM_element_has_no_children(element_id): void {
        let children = $("#" + element_id).children();
        if (children.length === 0) {
            return;
        } else {
            debugger;
            throw ("test_DOM_element_has_no_children failed " + element_id + " has " + children.length + " children");
        }
    }

    static is_defined(value): void {
        if (typeof value === "undefined") {
            debugger;
            throw ("is_defined failed");
        } else {
            return;
        }
    }

    static arrays_equal(array1, array2): void {
        if (Util.arrays_equal(array1, array2)) {
            return;
        } else {
            debugger;
            throw ("test_arrays_equal failed");
        }
    }
}
