(function test_is_different_object() {
    console.log("[TEST] testing test_is_different_object");
    let object1 = {};
    let object2 = {};

    Test.does_throw_error(Test.is_different_object.bind(null, object1, object1));
    Test.is_different_object(object1, object2);

    console.log("[TEST✔] test_is_different_object ran successfully");
})();
