/* globals app, Test, visualisation */
/* globals add_node, initial_layout */

/**
 * Tests adding new states and changing between them
 */
(function test_state_index(): void {
    console.log("[TEST] testing test_state_index");
    Test.expect(app.state.current_state_index, 0);
    Test.expect(app.state.layout.states.length, 1);

    app.set_state_index(0);
    Test.expect(app.state.current_state_index, 0);
    Test.expect(app.state.layout.states.length, 1);

    app.set_state_index(-1);
    Test.expect(app.state.current_state_index, 0);
    Test.expect(app.state.layout.states.length, 1);

    app.set_state_index(1);
    Test.expect(app.state.current_state_index, 0);
    Test.expect(app.state.layout.states.length, 1);

    app.append_new_state();
    Test.expect(app.state.current_state_index, 0);
    Test.expect(app.state.layout.states.length, 2);

    app.append_new_state();
    Test.expect(app.state.current_state_index, 0);
    Test.expect(app.state.layout.states.length, 3);

    app.set_state_index(1);
    Test.expect(app.state.current_state_index, 1);

    app.set_state_index(2);
    Test.expect(app.state.current_state_index, 2);

    // cleanup
    app.set_state_index(0);
    app.state.layout = initial_layout();
    Test.expect(app.state.current_state_index, 0);
    Test.expect(app.state.layout.states.length, 1);
    console.log("[TEST✔] test_state_index ran successfully");
})();

/**
 * Tests adding nodes to a state and changing states
 */
(function test_state_changes(): void {
    console.log("[TEST] testing test_state_changes");
    Test.expect(app.state.current_state_index, 0);
    Test.expect(app.state.layout.states.length, 1);
    Test.expect(visualisation.nodes.length, 0);
    Test.expect(visualisation.edges.length, 0);

    app.append_new_state();
    Test.expect(app.state.current_state_index, 0);
    Test.expect(app.state.layout.states.length, 2);

    add_node({ "id": 1, "label": "test" });
    add_node({ "id": 2, "label": "test2" });
    Test.expect(visualisation.nodes.length, 2);

    app.set_state_index(1);
    visualisation.load_current_state();
    Test.expect(app.state.current_state_index, 1);
    Test.expect(app.state.layout.states.length, 2);
    Test.expect(visualisation.nodes.length, 0);

    add_node({ "id": 1, "label": "test3" });
    add_node({ "id": 2, "label": "test4" });
    Test.expect(visualisation.nodes.length, 2);
    Test.expect(visualisation.edges.length, 0);

    app.state.layout = initial_layout();
    app.set_state_index(0);
    visualisation.nodes.clear();
    visualisation.edges.clear();
    Test.expect(app.state.current_state_index, 0);
    Test.expect(app.state.layout.states.length, 1);
    Test.expect(visualisation.nodes.length, 0);
    Test.expect(visualisation.edges.length, 0);
    console.log("[TEST✔] test_state_changes ran successfully");
})();

(function test_edit_current_state(): void {
    console.log("[TEST] testing edit_current_state");
    Test.expect(app.get_current_state().background, "default.png");

    app.edit_current_state("background", "background.png");
    Test.expect(app.get_current_state().background, "background.png");

    app.edit_current_state("background", "default.png");
    Test.expect(app.get_current_state().background, "default.png");
    console.log("[TEST✔] test_edit_current_state ran successfully");
})();

(function test_get_current_state(): void {
    console.log("[TEST] testing get_current_state");
    let state = app.get_current_state();
    Test.expect(state, app.state.layout.states[app.state.current_state_index]);
    console.log("[TEST✔] test_get_current_state ran successfully");
})();

(function test_set_background(): void {
    console.log("[TEST] testing set_background");
    Test.expect(app.get_current_state().background, "default.png");

    app.set_background("test_background.png");
    Test.expect(app.get_current_state().background, "test_background.png");

    console.log("[TEST✔] test_set_background ran successfully");
})();
