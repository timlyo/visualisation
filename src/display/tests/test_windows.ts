/// <reference path="_util.ts"/>
/// <reference path="../js/interface.ts"/>

/* globals Interface, app */
/* globals
	add_node
*/

(function open_all_windows(): void {
    console.log("[TEST] testing open_all_windows");
    test_open();
    test_save();
    test_background();
    test_icons();
    test_node_settings();
    test_edge_settings();
    console.log("[TEST✔] open_all_windows ran successfully");
})();

function test_open(): void {
    Interface.open_open();
    Test.expect(app.state.open_open, true);
    Test.test_DOM_element_not_null("openDimmer");

    Interface.close_open();
    Test.expect(app.state.open_open, false);
    Test.test_DOM_element_is_null("openDimmer");
}

function test_save(): void {
    Interface.open_save();
    Test.test_DOM_element_not_null("saveDimmer");
    Test.expect(app.state.save_open, true);

    Interface.close_save();
    Test.expect(app.state.save_open, false);
    Test.test_DOM_element_is_null("saveDimmer");
}

function test_background(): void {
    Interface.open_backgrounds();
    Test.test_DOM_element_not_null("backgroundsDimmer");
    Test.expect(app.state.background_open, true);

    Interface.close_backgrounds();
    Test.expect(app.state.background_open, false);
    Test.test_DOM_element_is_null("backgroundsDimmer");
}

function test_icons(): void {
    Interface.open_icons();
    Test.test_DOM_element_not_null("iconsDimmer");
    Test.expect(app.state.icons_open, true);

    Interface.close_icons();
    Test.expect(app.state.icons_open, false);
    Test.test_DOM_element_is_null("iconsDimmer");
}

function test_node_settings(): void {
    // node settings should never be null due to the way that the window system works
    // therefore a closed node_settings is one with no children*
    let element_id = "nodeSettings";

    Interface.close_node_settings();
    Interface.exit_edit_mode();

    Test.test_DOM_element_not_null(element_id);
    Test.test_DOM_element_has_no_children(element_id);

    Interface.open_node_settings();
    // app is not in edit mode, therefore node settings should not open
    Test.test_DOM_element_not_null(element_id);
    Test.test_DOM_element_has_no_children(element_id);

    Interface.enter_edit_mode();

    // no nodes are selected, node settings should not open
    Interface.open_node_settings();
    Test.test_DOM_element_not_null(element_id);
    Test.test_DOM_element_has_no_children(element_id);

    add_node({ id: "1" });
    visualisation.network.selectNodes("1");
    app.setState({ "selected_node_ids": visualisation.network.getSelectedNodes() });

    Interface.close_node_settings();
    Interface.open_node_settings();
    app.forceUpdate();
    // app is now in edit mode and nodes have been selected, node_settings should be open
    Test.test_DOM_element_not_null(element_id);
    Test.test_DOM_element_has_children(element_id);

    visualisation.nodes.clear();
    Interface.close_node_settings();
    Test.test_DOM_element_not_null(element_id);
    Test.test_DOM_element_has_no_children(element_id);
}

function test_edge_settings(): void {
    let element_id: string = "edgeSettings";

    Interface.close_edge_settings();
    Interface.exit_edit_mode();

    Test.test_DOM_element_not_null(element_id);
    Test.test_DOM_element_has_no_children(element_id);

    Interface.open_edge_settings();
    // app is not in edit mode, therefore node settings should not open
    Test.test_DOM_element_not_null(element_id);
    Test.test_DOM_element_has_no_children(element_id);

    Interface.enter_edit_mode();

    Interface.open_edge_settings();
    Test.test_DOM_element_not_null(element_id);
    Test.test_DOM_element_has_children(element_id);

    Interface.close_edge_settings();
    Test.test_DOM_element_not_null(element_id);
    Test.test_DOM_element_has_no_children(element_id);
}
