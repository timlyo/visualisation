/// <reference path="../js/util.ts"/>
/* globals Test, Util */

(function test_arrays_equal(): void {
    console.log("[TEST] testing test_arrays_equal");
    let array1 = [0, 1, 2];
    let array2 = [0, 1, 2];
    let array3 = [0, 1, 3];

    Test.expect(Util.arrays_equal(array1, array2), true);
    Test.expect(Util.arrays_equal(array1, array3), false);
    console.log("[TEST✔] test_arrays_equal ran successfully");
})();

(function test_unique(): void {
    console.log("[TEST] testing test_unique");
    let array1 = [0, 1, 2, 3];
    let result = Util.get_unique(array1);

    Test.arrays_equal(result, array1);
    console.log("[TEST✔] test_unique ran successfully");
})();
