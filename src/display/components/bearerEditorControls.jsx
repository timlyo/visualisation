/* globals React, app */
/* globals
	default_bearer
	apply_bearer_style
	delete_bearer
*/

/**
 * Contains all the functionality and layout for the bearer editing window
 * Represents one element that is contained within an accordion inside
 * BearerEditorWindow
 */
class BearerEditorControls extends React.Component {
	constructor() {
		super();

		this.on_name_change = this.on_name_change.bind(this);
		this.on_dashes_click = this.on_dashes_click.bind(this);
		this.on_color_change = this.on_color_change.bind(this);
		this.on_width_change = this.on_width_change.bind(this);

		this.delete_bearer = this.delete_bearer.bind(this);
	}

	on_name_change(event) {
		let bearers = app.state.layout.bearers;
		bearers[this.props.index].name = event.target.value;
		app.modify_layout({"bearers": bearers});

		apply_bearer_style();
	}

	on_dashes_click(event) {
		let bearers = app.state.layout.bearers;
		bearers[this.props.index].style.dashes = event.target.checked;
		app.modify_layout({"bearers": bearers});

		apply_bearer_style();
	}

	on_color_change(event){
		let bearers = app.state.layout.bearers;
		bearers[this.props.index].style.color = event.target.value;
		app.modify_layout({"bearers": bearers});

		apply_bearer_style();
	}

	on_width_change(event){
		let bearers = app.state.layout.bearers;
		bearers[this.props.index].style.width = parseFloat(event.target.value);
		app.modify_layout({"bearers": bearers});

		apply_bearer_style();
	}

	delete_bearer(event){
		let name = this.props.bearer.name;
		delete_bearer(name);
	}

	render() {
		let bearer = this.props.bearer;
		let dashes = bearer.style.dashes;
		let colour = bearer.style.color;
		let width = bearer.style.width;

		return (
			<div class="bearerEditorControls">
				<div className="title">
					{bearer.name}
					<button className="ui icon button floatRight noPadding" onClick={this.delete_bearer}>
						<i className="ui trash icon"></i>
					</button>
				</div>

				<div className="content">

					<div className="ui form">

						<div className="field">
							<label>Name</label>
							<input type="text" className="noSidePadding" value={bearer.name} onChange={this.on_name_change}/>
						</div>

						<div className="field">
							<label>Dashes</label>
							<input type="checkbox" onChange={this.on_dashes_click} checked={dashes}/>
						</div>

						<div className="field">
							<label>Colour</label>
							<input type="color" className="ui input" value={colour} onChange={this.on_color_change}/>
						</div>

						<div className="field">
							<label>Width {width}</label>
						<input type="range" min="0.1" max="5" step="0.1" value={width} onChange={this.on_width_change}/>
						</div>

					</div>

				</div>
			</div>
		);
	}
}

BearerEditorControls.propTypes = {
	bearer: React.PropTypes.object.isRequired,
	index: React.PropTypes.number.isRequired
};
