/* globals React */

class IconItem extends React.Component {
	get_toggle(state) {
		if(typeof state === "undefined"){
			return null;
		}

		if (state) {
			return <i className="toggle on icon"></i>;
		} else {
			return <i className="toggle off icon"></i>;
		}
	}

	render() {
		let toggle = this.get_toggle(this.props.toggle);

		return (
			<a className="item" onClick={this.props.action}>
				{toggle}
				<i className={this.props.icon + " icon"}/> {this.props.text}
			</a>
		);
	}
}

IconItem.propTypes = {
	text: React.PropTypes.string.isRequired,
	action: React.PropTypes.func.isRequired,
	icon: React.PropTypes.string.isRequired,
	toggle: React.PropTypes.bool
};
