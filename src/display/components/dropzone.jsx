/*globals React, ReactDOM, Interface */

class Dropzone extends React.Component {
	componentDidMount(){
		Interface.init_dropzone();
	}

	render() {
		return (
			<div>
				<form action={this.props.action} className="dropzone">
					<span className="dz-message">{this.props.text}</span>
				</form>
			</div>
		);
	}
}
