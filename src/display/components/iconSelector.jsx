/*globals React, ReactDOM, app, visualisation */
/* globals set_selected_nodes_image */

class IconModal extends React.Component {
	render() {
		if (!this.props.icons_open) {
			return null;
		}

		let icons_in_use = ((node_ids) => {
			let icons = visualisation.nodes.get(node_ids);

			return icons.map(function(icon, index){
				if (icon.image === undefined){
					return undefined;
				}
				let url_parts = icon.image.split("/");
				return "/api/icon/" + url_parts[url_parts.length-1];
			});
		})(this.props.selected_node_ids);

		return (
			<div className="ui active dimmer dialog" id="iconsDimmer">
				<div className="content">
					<IconContent icons={this.props.icons} current_icons={icons_in_use}/>
				</div>
			</div>
		);
	}
}

IconModal.propTypes = {
	icons_open: React.PropTypes.bool.isRequired,
	icons: React.PropTypes.arrayOf(React.PropTypes.string).isRequired,
	selected_node_ids: React.PropTypes.array.isRequired
};

class IconContent extends React.Component {
	on_image_click(event) {
		let image_url_sections = event.target.src.split("/");
		let image_name = "/api/icon/" + image_url_sections[image_url_sections.length-1];

		console.log("name", image_name);
		set_selected_nodes_image(image_name);
		app.forceUpdate(); // to re-render
	}

	render() {
		return (
			<div className="ui grid">
				<div className="fourteen wide column">
					<div className="ui segment grid">
						<TitleBar title={"Icon"}/>

						<div className="ui divider"></div>

						<div className="row">
							<div className="column">
								<ImageGrid images={this.props.icons} on_image_click={this.on_image_click} emphasised_images={this.props.current_icons}/>
							</div>
						</div>

						<div className="ui divider"></div>

						<div className="row">
							<div className="column">
								<Dropzone action="/api/icon" text="Drop icons here or click to upload"/>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

IconContent.propTypes = {
	icons: React.PropTypes.arrayOf(React.PropTypes.string).isRequired,
	current_icons: React.PropTypes.array.isRequired
};
