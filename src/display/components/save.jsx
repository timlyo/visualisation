/*globals React, app Data, Interface*/

function file_exists(name) {
	let files = app.state.files.map((file) => file.name);
	if(files.length == 0){
		console.warn("file_exists function found 0 files");
	}
	return $.inArray(name, files) != -1;
}

class SaveModal extends React.Component {
	render() {
		if (this.props.open) {
			return(
				<div className="ui active dimmer dialog" id="saveDimmer">
					<div className="content">
						<SaveContent initial_name={this.props.initial_name}/>
					</div>
				</div>
			);

		}else{
			return null;
		}
	}
}

SaveModal.propTypes = {
	open: React.PropTypes.bool.isRequired,
	initial_name: React.PropTypes.string.isRequired,
};

class SaveContent extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			overwriting: false,
			input_value: this.props.initial_name,
		};

		this.handle_name_change = this.handle_name_change.bind(this);
		this.save = this.save.bind(this);
		this.check_for_overwrite = this.check_for_overwrite.bind(this);
	}

	componentDidMount() {
		Data.update_file_list();

		this.check_for_overwrite(this.props.initial_name);
	}

	handle_name_change(event) {
		let name = event.target.value;
		this.check_for_overwrite(name);
	}

	check_for_overwrite(name){
		this.setState({"input_value": name});
		if(file_exists(name)){
			this.setState({"overwriting": true});
		}else{
			this.setState({"overwriting": false});
		}
	}

	save(event){
		let name = this.state.input_value;
		app.modify_layout({"name": name});
		Interface.close_save();
		Data.post_layout_to_server();
	}

	get_save_button(overwriting){
		if (this.state.overwriting) {
			return (
				<button className="ui orange button labelled" onClick={this.save}>
					<i className="save icon"/>
					Overwrite
				</button>
			);
		} else {
			return (
				<button className="ui button primary labelled" onClick={this.save}>
					<i className="save icon"/>
					Save
				</button>
			);
		}
	}

	render() {
		let save_button = this.get_save_button(this.state.overwriting);

		return (
			<div className="ui grid">
				<div className="twelve wide column">
					<div className="ui segment two column grid">
						<TitleBar title={"Save"}/>

						<div className="ui divider"></div>

						<div className="row">
							<div className="column">
								<form className="ui form">
									<div className="field">
										<input value={this.state.input_value} type="text" onChange={this.handle_name_change} autoFocus id="saveInput"/>
									</div>
								</form>
							</div>
						</div>

						<div className="ui divider"></div>

						<div className="row">
							<div className="column">

								{save_button}

								<button className="ui button">
									Cancel
								</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}
