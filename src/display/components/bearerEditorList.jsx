/* globals React */


class BearerEditorList extends React.Component{
	render_empty(){
		return(
			<p>No Bearers have been created</p>
		);
	}

	render_list(){
		let list = this.props.bearers.map((bearer, index) => {
			return <BearerEditorControls bearer={bearer} index={index} key={index}/>;
		});

		return(
			<div className="ui styled accordion" id="bearersAccordion">
				{list}
			</div>
		);
	}

	render(){
		let bearers = this.props.bearers;

		if (bearers.length == 0){
			return this.render_empty();
		}else{
			return this.render_list();
		}
	}
}

BearerEditorList.PropTypes = {
	bearers: React.PropTypes.array.isRequired
};
