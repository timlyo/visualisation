/*globals React, ReactDOM */

class ImageGrid extends React.Component {
	render() {
		let on_image_click = this.props.on_image_click;
		if (typeof on_image_click === "undefined") {
			on_image_click = function() {};
		}

		if (this.props.images === undefined) {
			console.error("Images prop is undefined:", this.props.images);
		}

		let emphasised_images = this.props.emphasised_images;
		if (emphasised_images === undefined) {
			emphasised_images = [];
		}

		let column_count = this.props.column_count;

		let images = this.props.images.map(function(image, index) {
			let emphasised = emphasised_images.indexOf(image) !== -1;
			return (<ImageComponent image={image} on_image_click={on_image_click} emphasised={emphasised} column_count={column_count}/>);
		});

		return (
			<div className="ui equal height grid">
				{images}
			</div>
		);
	}
}

ImageGrid.propTypes = {
	images: React.PropTypes.array.isRequired,
	emphasised_images: React.PropTypes.array.isRequired,
	column_count: React.PropTypes.string
};

class ImageComponent extends React.Component {
	format_column_count(count) {
		if (count === undefined) {
			return "four";
		}

		if (typeof(count) === "string") {
			return count;
		}
	}

	render() {
		let image = this.props.image;
		let emph = this.props.emphasised
			? " emphasised "
			: "";
		let count = this.format_column_count(this.props.column_count);

		return (
			<div className={count + " wide column"}>
				<img src={image} className={"ui image" + emph} onClick={this.props.on_image_click}></img>
			</div>
		);
	}
}

ImageComponent.propTypes = {
	on_image_click: React.PropTypes.func.isRequired,
	image: React.PropTypes.string.isRequired,
	emphasised: React.PropTypes.bool.isRequired,
	column_count: React.PropTypes.string
};
