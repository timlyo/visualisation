/*globals React, Data, Interface, visualisation, app*/

class OpenModal extends React.Component {
	componentDidMount() {
		Data.update_file_list();
	}

	render() {
		if (this.props.open) {
			return (
				<div className="ui active dimmer dialog" id="openDimmer">
					<div className="content">
						<OpenContent files={this.props.files}/>
					</div>
				</div>
			);

		} else {
			return null;
		}
	}
}

OpenModal.propTypes = {
	open: React.PropTypes.bool.isRequired,
	files: React.PropTypes.array.isRequired
};

class OpenContent extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			active: 0
		};
	}

	set_active(index) {
		this.setState({active: index});
	}

	load_active() {
		let active_index = this.state.active;
		let file = app.state.files[active_index];

		Data.load_layout(file.name);

		Interface.close_open();
		visualisation.load_current_state();
	}

	file_box(active_file){
		if (this.props.files.length == 0 || this.props.files == null) {
			return (
				<div className="ui active inverted dimmer" id="fileBox">
					<div className="ui loader"></div>
				</div>
			);
		}else{
			return (
				<div className="row" id="fileBox">
					<div className="column">
						<FileList files={this.props.files} active={this.state.active} set_active={this.set_active} context={this}/>
					</div>
					<div className="column">
						<FileDetails file={active_file}/>
						<OpenButtons name={active_file.name}/>
					</div>
				</div>
			);
		}
	}

	render() {
		let active_file = this.props.files[this.state.active];
		// if(typeof active_file === "undefined" || active_file === null){
		// 	console.error("Active file is", active_file, "in OpenContent");
		// 	console.error("Files=", this.props.files);
		// }

		let file_box = this.file_box(active_file);

		return (
			<div className="ui grid">
				<div className="twelve wide column">
					<div className="ui segment equal width grid">

						{/*Title Bar*/}
						<TitleBar title={"Open"} />

						<div className="ui divider"></div>

						{file_box}

						<div className="ui divider"></div>

						{/*Bottom buttons*/}
						<div className="row">
							<div className="column">
								<button className="ui primary button labelled" onClick={this.load_active.bind(this)}>
									<i className="folder open icon"/>
									Open
								</button>

								<button className="ui button" onClick={Interface.close_open}>
									Cancel
								</button>
							</div>
						</div>

					</div>
				</div>
			</div>
		);
	}
}

OpenContent.propTypes = {
	files: React.PropTypes.array.isRequired
};

/**
 * The list of files.
 * Composed of File objects
 */
class FileList extends React.Component {
	render() {
		let active = this.props.active;
		let set_active = this.props.set_active;
		let context = this.props.context;

		let files = this.props.files.map(function(file, index) {
			return (<File file={file} key={file.name} active={index == active} set_active={set_active.bind(context, index)}/>);
		});

		return (
			<table className="ui celled table">
				<thead>
					<tr>
						<td>Name</td>
						<td>Date</td>
					</tr>
				</thead>
				<tbody>
					{files}
				</tbody>
			</table>
		);
	}
}

FileList.propsTypes = {
	files: React.PropTypes.array.isRequired
};

class File extends React.Component {
	render() {
		let row_class = "";
		if (this.props.active) {
			row_class = "active";
		}
		return (
			<tr className={row_class} onClick={this.props.set_active}>
				<td>{this.props.file.name}</td>
				<td>{this.props.file.date}</td>
			</tr>
		);
	}
}

File.propTypes = {
	file: React.PropTypes.object.isRequired
};

/**
 * Box on the right that displays the information about the file
 */
class FileDetails extends React.Component {
	render() {
		let file = this.props.file;
		if (typeof file === "undefined") {
			return <div className="ui segment"></div>;
		} else {
			return (
				<div className="ui secondary segment" id="details_box">
					<h3 className="ui header">Details</h3>
					<div className="ui list">
						<div className="item">Name: {file.name}</div>
						<div className="item">Date: {file.date}</div>
						<div className="item">{file.description}</div>
						{/*<div className="item">{file.}</div>*/}
					</div>
				</div>
			);
		}
	}
}

FileDetails.propTypes = {
	file: React.PropTypes.object.isRequired
};

class OpenButtons extends React.Component {
	constructor(props){
		super(props);

		this.delete_layout = this.delete_layout.bind(this);
	}

	delete_layout(event){
		Data.delete_layout(this.props.name);
	}

	render(){
		return(
			<div>
				<button className="ui button" onClick={this.delete_layout}>Delete</button>
				<button className="ui button">Rename</button>
			</div>
		);
	}
}

OpenButtons.PropTypes = {
	name: React.PropTypes.string.isRequired
};
