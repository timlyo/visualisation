/* globals React, Interface, Util, visualisation */
/* globals
	set_selected_edges_bearer
	apply_bearer_style
*/

class EdgeSettings extends React.Component {
	render() {
		if (!this.props.open) {
			return (
				<div id="edgeSettings"></div>
			);
		}

		let edges = visualisation.edges.get(this.props.selected_edges);

		let bearers = edges.map((edge) => edge.bearer);
		let unique_bearers = Util.get_unique(bearers);

		return (
			<div id="edgeSettings" className="window">
				<div className="ui top attached menu draggable">
					<div className="item">Edge Settings</div>

					<div className="right menu">
						<a className="item" onClick={Interface.close_edge_settings}>
							<i className="close icon"/>
						</a>
					</div>

				</div>
				<div className="ui bottom attached segment">
					<EdgeSettingsForm bearers={this.props.bearers} current_bearers={unique_bearers}/>
				</div>
			</div>
		);
	}
}

EdgeSettings.propTypes = {
	selected_edges: React.PropTypes.arrayOf(React.PropTypes.string).isRequired,
	open: React.PropTypes.bool.isRequired,
	bearers: React.PropTypes.array.isRequired
};

class EdgeSettingsForm extends React.Component {
	on_bearer_select(event) {
		let bearer_name = event.target.value;

		if (bearer_name == "default") {
			set_selected_edges_bearer(null);
		} else {
			set_selected_edges_bearer(bearer_name);
		}

		apply_bearer_style();
	}

	render() {
		let current_bearers = this.props.current_bearers;
		let default_value = "Default";

		if(current_bearers.length === 1){
			default_value = current_bearers[0];
		}

		let bearers_options = this.props.bearers.map((bearer) => {
			return (
				<option value={bearer.name} key={bearer.name}>
					{bearer.name}
				</option>
			);
		});

		bearers_options.push(
			<option value={"Default"} key={"Default"}>
				Default
			</option>
		);

		return (
			<div>
				<label>Bearer</label>
				<select onChange={this.on_bearer_select} defaultValue={default_value}>
					{bearers_options}
				</select>
			</div>
		);
	}
}

EdgeSettingsForm.propTypes = {
	bearers: React.PropTypes.array.isRequired,
	current_bearers: React.PropTypes.arrayOf(React.PropTypes.string).isRequired
};
