/*globals React, visualisation, app, Interface */

/**
 * This is the window that will appear on the side of the app, similar to the slide selector in powerpoint.
 * It will allow the user to add, delete and reorder states.
 */
class StateSelector extends React.Component {
	set_state(index) {
		if (app.set_state_index(index)) {
			visualisation.load_current_state();
		}
	}

	/**
	 * Return a list of html elements that will make up the contents of the selector
	 * @param  {Array[object]} 	states					An Array of state objects
	 * @param  {int} 						current_index 	The index of the currently selected state.
	 * @return {html}														The html elements
	 */
	get_state_elements(states, current_index) {
		if ((typeof states == "undefined") || (states === null)) {
			return null;
		}

		return this.props.states.map((state, index) => {
			let active = (index == current_index);
			return (<State key={index} state={state} active={active} index={index} set_state={this.set_state.bind(this, index)}/>);
		});
	}

	render() {
		let set_state = this.set_state;
		let content = this.get_state_elements(this.props.states, this.props.current_state_index);

		if (this.props.open) {
			return (
				<div className="ui inverted left attached vertical menu" id="stateSelector">
					<button className="ui inverted icon button" onClick={Interface.close_sidebar}>
						<i className="angle left icon"/>
					</button>

					{content}

					<a className="item" onClick={app.append_new_state}>
						<i className="add square icon"></i>
						New State
					</a>
				</div>
			);
		} else {
			return null;
		}
	}
}

StateSelector.propTypes = {
	states: React.PropTypes.arrayOf(React.PropTypes.object).isRequired,
	current_state_index: React.PropTypes.number.isRequired,
	open: React.PropTypes.bool.isRequired
};

class State extends React.Component {
	render() {
		let active = "";
		if (this.props.active) {
			active = "active";
		}

		return (
			<a className={active + " item"} onClick={this.props.set_state}>
				{this.props.index}
				<i className="trash"></i>
			</a>
		);
	}
}

State.propTypes = {
	state: React.PropTypes.object,
	set_state: React.PropTypes.func.isRequired,
	index: React.PropTypes.number.isRequired,
	active: React.PropTypes.bool.isRequired
};
