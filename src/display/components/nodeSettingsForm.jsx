/*globals React, Interface, Util */
/* globals
	delete_selected_nodes
	set_selected_nodes_fixed
	set_selected_nodes_label
	set_selected_nodes_physics
	set_selected_nodes_size
	set_selected_nodes_router
*/

function get_node_label_denominator(nodes) {
	let labels = Util.get_unique(nodes.map((node) => node.label));
	if (labels.length === 1) {
		if (labels[0] === undefined) {
			return "";
		} else {
			return labels[0];
		}
	} else {
		return "";
	}
}

function get_node_fixed_denominator(nodes) {
	let physics = Util.get_unique(nodes.map((node) => node.fixed == true));
	let fixed = false;
	let fixed_indeterminate = false;

	if (physics.length == 1) {
		fixed = physics[0];
	} else {
		fixed_indeterminate = true;
	}

	return {fixed: fixed, fixed_indeterminate: fixed_indeterminate};
}

/**
 * Settings that are shared when multiple or single nodes are selected
 */
class NodeSettingsForm extends React.Component {
	constructor() {
		super();

		this.state = {
			label_name: ""
		};

		this.on_fixed_checkbox_click = this.on_fixed_checkbox_click.bind(this);
	}

	on_slider_change(event) {
		set_selected_nodes_size(event.target.value);
	}

	on_label_change(event) {
		let label = event.target.value;
		this.setState({"label_name": label});
		set_selected_nodes_label(label);
	}

	on_fixed_checkbox_click(event) {
		set_selected_nodes_fixed(event.target.checked);
		this.forceUpdate();
	}

	on_router_id_click(event){
		set_selected_nodes_router(event.target.value);
	}

	render() {
		let nodes = this.props.nodes;

		let routers = this.props.routers.map((router) => {
			return <option
								value={router.id}
								key={router.id}
							>{router.id}</option>;
		});
		routers.push(<option value={"None"}>None</option>);

		let label = get_node_label_denominator(this.props.nodes);
		let physics = get_node_fixed_denominator(nodes);

		return (
			<div>
				<div className="column">
					<label>Label</label>
					<input className="ui input" onChange={this.on_label_change.bind(this)} defaultValue={label}/>
				</div>

				<div className="column">
					<button className="ui button" onClick={Interface.open_icons}>Set Icon</button>
					<button className="ui button" onClick={delete_selected_nodes}>Delete</button>
				</div>

				<div className="column">
					<label>Router ID</label>
					<select className="ui dropdown" onChange={this.on_router_id_click} defaultValue="None">
						{routers}
					</select>
				</div>

				<div className="column">
					<label>Scale</label>
					<input type="range" onChange={this.on_slider_change} min="0.1"></input>
				</div>

				<div className="column">
					<label>Fixed</label>
					<input type="checkbox" onClick={this.on_fixed_checkbox_click} checked={physics.fixed} ref={input => {
						if (input) {
							input.indeterminate = this.props.fixed_indeterminate;
						}
					}}/>
				</div>

			</div>
		);
	}
}

NodeSettingsForm.propTypes = {
	nodes: React.PropTypes.array.isRequired,
	routers: React.PropTypes.array.isRequired,
};
