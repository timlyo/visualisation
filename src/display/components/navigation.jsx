/*globals React, visualisation, Interface, app*/
/* globals
	new_node
	link_selected
*/

class Navigation extends React.Component {
	render() {
		if (this.props.edit_mode) {
			return (
				<div id="menu">
					<EditMenu/>
				</div>
			);
		} else {
			return (
				<div id="menu">
					<PresentMenu/>
				</div>
			);
		}
	}
}

Navigation.propTypes = {
	edit_mode: React.PropTypes.bool.isRequired
};

class EditMenu extends React.Component {
	render() {
		return (
			<div className="ui top attached menu">
				<FileDropdown key={"fileDropdown"}/>
				<ViewDropdown/>
				<EditDropdown/>

				<div className="item popup" data-content="Add new node" onClick={new_node}>
					<i className="plus icon"/>
				</div>
				<div className="item popup" data-content="Link up selected nodes" onClick={link_selected}>
					<i className="linkify icon"/>
				</div>

				<div className="right menu">
					<a className="item button" onClick={Interface.exit_edit_mode}>
						Present
					</a>
				</div>
			</div>
		);
	}
}

class PresentMenu extends React.Component {
	componentDidMount() {
		$(".ui.dropdown").dropdown();
	}

	render() {
		return (
			<div className="ui top attached menu">
				<FileDropdown key={"FileDropdown"}/>
				<ViewDropdown/>

				<div className="right menu">
					<a className="item button" onClick={Interface.enter_edit_mode}>
						Edit
					</a>
				</div>
			</div>
		);
	}
}

class FileDropdown extends React.Component {
	render() {
		return (
			<div className="ui dropdown item">
				Menu
				<i className="dropdown icon"/>
				<div className="menu">
					<IconItem text={"Save"} icon={"save"} action={Interface.open_save} />
					<IconItem text={"Open"} icon={"folder open"} action={Interface.open_open} />
					<IconItem text={"New"} icon={"folder outline"} action={visualisation.clear} />
					<div className="ui divider"></div>
					<IconItem text={"Settings"} icon={"settings"} action={Interface.open_settings} />
				</div>
			</div>
		);
	}
}

class EditDropdown extends React.Component {
	componentDidMount() {
		$(".ui.dropdown").dropdown();
	}

	render() {
		return (
			<div className="ui dropdown item">
				Edit
				<i className="dropdown icon"/>
				<div className="menu">
					<IconItem text={"Change State's Background"} icon={"picture"} action={Interface.open_backgrounds} />
					<IconItem text={"Edit Bearers"} icon={"RSS"} action={Interface.open_bearer_editor} />
				</div>
			</div>
		);
	}
}

class ViewDropdown extends React.Component {
	componentDidMount() {
		$(".ui.dropdown").dropdown();
	}

	render() {
		let bearer_window_state = app ? app.state.bearers_open: undefined;

		return (
			<div className="ui dropdown item">
				View
				<i className="dropdown icon"/>
				<div className="menu">
					<IconItem text={"Bearer Key"} icon={""} action={Interface.toggle_bearer_window} toggle={bearer_window_state}/>
				</div>
			</div>
		);
	}
}
