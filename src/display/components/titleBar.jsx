/* globals React */

class TitleBar extends React.Component {
	render() {
		return (
			<div className="row">
				<div className="column">
					<h2 className="ui header">{this.props.title}</h2>
				</div>
			</div>
		);
	}
}

TitleBar.propTypes = {
	title: React.PropTypes.string.isRequired
};
