/*globals React, ReactDOM, visualisation, types, app */

class BackgroundModal extends React.Component {
	render() {
		if (this.props.open) {
			return (
				<div className="ui active dimmer dialog" id="backgroundsDimmer">
					<div className="content">
						<BackgroundContent store={this.props.backgrounds} backgrounds={this.props.backgrounds} current_background={this.props.current_background}/>
					</div>
				</div>
			);
		} else {
			return null;
		}
	}
}

BackgroundModal.propTypes = {
	open: React.PropTypes.bool.isRequired,
	backgrounds: React.PropTypes.arrayOf(React.PropTypes.string).isRequired,
	current_background: React.PropTypes.string.isRequired
};

class BackgroundContent extends React.Component {
	set_background(event) {
		let url_sections = event.target.src.split("/");
		let name = url_sections[url_sections.length - 1];
		let index = app.state.current_state_index;

		app.edit_current_state("background", name);

		console.log("Setting background to: ", name);
		visualisation.set_background();
	}


	render() {
		let current_background = "/api/background/" + this.props.current_background;

		return (
			<div className="ui grid">
				<div className="twelve wide column">
					<div className="ui segment grid">
						<TitleBar title={"Backgrounds"} />

						<div className="ui divider"></div>

						<div className="row">
							<div className="column">
								<ImageGrid images={this.props.backgrounds} on_image_click={this.set_background} emphasised_images={[current_background]}/>
							</div>
						</div>

						<div className="ui divider"></div>

						<div className="row">
							<div className="column">
								<Dropzone action="/api/background" text="Drop backgrounds here or click to upload"/>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

BackgroundContent.propTypes = {
	backgrounds: React.PropTypes.arrayOf(React.PropTypes.string).isRequired,
	current_background: React.PropTypes.string.isRequired
};
