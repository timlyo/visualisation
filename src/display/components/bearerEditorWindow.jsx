/* globals React, app */
/* globals
	default_bearer
	apply_bearer_style
	uniqueify_bearer_name
*/

/**
 * Modal containing the controls for editing bearer information
 */
class BearerEditorWindow extends React.Component {
	constructor() {
		super();

		this.add_bearer = this.add_bearer.bind(this);
	}

	componentDidUpdate() {
		$("#bearersAccordion").accordion();
	}

	add_bearer(event) {
		let bearers = app.state.layout.bearers;
		let new_bearer = default_bearer();
		let bearer_names = app.state.layout.bearers.map((bearer) => bearer.name);
		let name = uniqueify_bearer_name(new_bearer.name, bearer_names);
		new_bearer.name = name;

		bearers.push(new_bearer);
		app.modify_layout({"bearers": bearers});
	}

	render() {
		if (this.props.open) {
			return (
				<div className="ui active dimmer dialog" id="bearerDimmer">
					<div className="content">
						<div className="ui grid">
							<div className="twelve wide column">
								<div className="ui segment">
									<TitleBar title={"Bearers"}/>

									<button className="ui button" onClick={this.add_bearer}>Add Bearer</button>

									<BearerEditorList bearers={this.props.bearers}/>

								</div>
							</div>
						</div>
					</div>
				</div>
			);
		} else {
			return null;
		}
	}
}

BearerEditorWindow.propTypes = {
	open: React.PropTypes.bool.isRequired,
	bearers: React.PropTypes.array.isRequired
};
