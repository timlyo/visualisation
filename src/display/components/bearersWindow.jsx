/*globals React*/
/* globals
	complete_bearer_style
*/

/**
 * Draws a line on the supplied canvas
 * @param  {string} canvas_id The id of the canvas to be drawn to
 * @param  {object} style     object containing line styles
 */
function drawLine(canvas_id, style) {
	let canvas = document.getElementById(canvas_id);
	if (canvas === null) {
		return;
	}
	let ctx = canvas.getContext("2d");
	ctx.clearRect(0, 0, canvas.width, canvas.height);

	style = complete_bearer_style(style);

	if (style.dashes) {
		ctx.setLineDash([10, 15]);
	} else {
		ctx.setLineDash([]);
	}

	ctx.lineWidth = style.width;

	ctx.strokeStyle = style.color;

	ctx.beginPath();
	ctx.moveTo(0, 5);
	ctx.lineTo(100, 5);

	ctx.stroke();
}

class Bearers extends React.Component {
	constructor() {
		super();
		this.id = "bearers";
	}

	componentDidUpdate() {
		for (let bearer of this.props.bearers) {
			let id = bearer.name + "BearerCanvas";
			drawLine(id, bearer.style);
		}
	}

	render() {
		if (!this.props.open) {
			return (
				<div id={this.id}></div>
			);
		}

		let bearer_list = null;
		if (this.props.bearers.length > 0) {
			bearer_list = <BearerList bearers={this.props.bearers}/>;
		} else {
			bearer_list = <EmptyBearerList/>;
		}

		return (
			<div id={this.id} className="window">
				<div className="ui top attached menu draggable">
					<div className="item">Bearers</div>
				</div>

				<div className="ui bottom attached segment">
					{bearer_list}
				</div>
			</div>
		);
	}
}

class BearerList extends React.Component {
	render() {
		let bearers = this.props.bearers.map((bearer, index) => {
			return <Bearer name={bearer.name} key={index}/>;
		});

		return (
			<div className="ui three column grid">
				{bearers}
			</div>
		);
	}
}

BearerList.propTypes = {
	bearers: React.PropTypes.array.isRequired
};

class Bearer extends React.Component {
	render() {
		return (
			<div className="row">
				<div className="column">
					{this.props.name}
				</div>

				<div className="column">
					<canvas id={this.props.name + "BearerCanvas"} className="bearerLineCanvas" width="100px" height="10px"/>
				</div>

			</div>
		);
	}
}

Bearer.propTypes = {
	name: React.PropTypes.string.isRequired
};

class EmptyBearerList extends React.Component {
	render() {
		return ( <p className="greyedOut"> No bearers</p>);
	}
}

EmptyBearerList.propTypes = {};
