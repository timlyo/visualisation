/*globals React, visualisation, ReactDOM, Interface */
/* globals
	delete_selected_nodes
	set_selected_nodes_fixed
	set_selected_nodes_label
	set_selected_nodes_physics
	set_selected_nodes_size
*/

class NodeSettingsWindow extends React.Component {
	constructor() {
		super();

		this.empty_element = <div id="nodeSettings"></div>;
	}

	render() {
		let nodes = visualisation.nodes.get(this.props.selected_nodes);

		if (nodes.length == 0) {
			return this.empty_element;
		}

		if (nodes.filter((node) => node === null).length > 0) {
			return this.empty_element;
		}

		if (!this.props.open) {
			return (
				<div id="nodeSettings"></div>
			);
		} else {
			return (
				<div id="nodeSettings" className="window">
					<div className="ui top attached menu draggable">
						<div className="item">Node Settings</div>

						<div className="right menu">
							<a className="item" onClick={Interface.close_node_settings}>
								<i className="close icon"/>
							</a>
						</div>
					</div>

					<div className="ui bottom attached segment">
						<NodeSettingsForm nodes={nodes} routers={this.props.routers}/>
					</div>

				</div>
			);
		}
	}
}

NodeSettingsWindow.propTypes = {
	selected_nodes: React.PropTypes.arrayOf(React.PropTypes.string),
	open: React.PropTypes.bool.isRequired,
	routers: React.PropTypes.arrayOf(React.PropTypes.object)
};
