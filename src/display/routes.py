from display import app, web_socket, msg
from flask import render_template, jsonify, request, send_from_directory, abort

from flask_socketio import emit, send

import time
import display.data

import pprint
import threading

printer = pprint.PrettyPrinter().pprint
controller_thread = None

@app.route("/")
def index_route():
    test = request.args.get("test") == "true"

    return render_template("index.html", test=test)

@web_socket.on("connect")
def web_socket_connected():
    global controller_thread

    if controller_thread is None:
        print("Starting controller_thread")
        controller_thread = web_socket.start_background_task(target=msg.listen)

@web_socket.on("my event")
def handle_event():
    print("my event")
    send("Contoller event")

@web_socket.on("message")
def handle_message(message):
    print("WS Message:", message)
    web_socket.send("Message")

@app.route("/api/nodes", methods=["GET"])
def get_nodes():
    return jsonify(data=nodes)


@app.route("/api/load/", methods=["GET"])
def get_file_list():
    file_list = display.data.get_save_list()
    return jsonify({"data": file_list})


@app.route("/api/load/<id>", methods=["GET"])
def load(id):
    print("Loading:", id)
    contents = display.data.get_file_by_name(id)
    if contents:
        return jsonify({"data": contents})
    else:
        return "Not found", 404


@app.route("/api/load/<id>", methods=["DELETE"])
def delete(id):
    print("Deleting:", id)
    try:
        display.data.delete_layout(id)
    except FileNotFoundError as e:
        return "File not found", 404
    return "ok", 200


@app.route("/api/save/<id>", methods=["POST"])
def save(id):
    print("Saving:", id)
    layout = request.get_json()
    printer(layout)
    try:
        display.data.save_layout(layout)
    except KeyError as e:
        print("Error saving layout", e)
        return str(e), 500

    print("Layout saved")
    return "ok"


@app.route("/api/background", methods=["POST"])
def save_background():
    """Upload a new background"""
    file = request.files["file"]
    if file.mimetype.startswith("image/"):
        try:
            display.data.save_background(file)
            return "ok", 201
        except ValueError as e:
            return "{}".format(e), 409
        except OSError as e:
            return "{}".format(e), 500
    else:
        return "File must be an image", 400


@app.route("/api/background", methods=["GET"])
def get_backgrounds():
    """ Get a list of backgrounds """
    result = display.data.get_background_list()
    return jsonify(data=result)


@app.route("/api/background/<path:id>", methods=["GET"])
def get_background(id):
    """ Load a single background image"""
    return send_from_directory(display.data.backgrounds_dir, id)


@app.route("/api/icon", methods=["GET"])
def get_icon_list():
    """Get a list of icons"""
    result = display.data.get_icon_list()
    return jsonify(data=result)


@app.route("/api/icon/<id>", methods=["GET"])
def get_icon(id):
    """Load a single icon image"""
    if '..' in id or id.startswith('/'):
        abort(404)

    return send_from_directory(display.data.icons_dir, id)


@app.route("/api/icon", methods=["POST"])
def save_icon():
    """Save an icon"""
    file = request.files["file"]
    if file.mimetype.startswith("image/"):
        try:
            display.data.save_icon(file)
            print("Saved icon:", file.filename)
            return "ok", 201
        except ValueError as e:
            return "{}".format(e), 409
        except OSError as e:
            return "{}".format(e), 500
    else:
        return "File must be an image", 400
