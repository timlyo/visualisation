/*globals React, Cookies, Visualisation, Data, Layer*/
/* globals
	default_bearer
*/

console.log("Running app");
let visualisation = new Visualisation();

/**
 * Creates a new empty layout
 * @return {object} [Empty layout]
 */
function initial_layout(){
	let layout = {
		name: "New Layout",
		bearers: [],
		states: [
			{
				background: "default.png",
				nodes: [],
				edges: []
			}
		]
	};

	// check that initial_layout isn't malformed
	if(layout.states[0].nodes.length > 0){
		console.warn("Empty layout contains nodes");
	}

	if(layout.states[0].edges.length > 0){
		console.warn("Empty layout contains edges");
	}

	layout.bearers.push(default_bearer());

	return layout;
}

class App extends React.Component {
	constructor(props) {
		super(props);

		let edit_mode = Cookies.get("edit_mode") === "true";

		/**
		 * Central store for any non-visualisation data, must not be updated directly
		 * https://facebook.github.io/react/docs/component-api.html#setstate
		 */
		this.state = {
			background_open: false, 		// whether the background selection window is open
			backgrounds: [], 						// list of all available backgrounds
			bearers_open: false,				// Whether the bearers window is open
			bearer_editor_open: false,	// Whether the bearers editing window is open
			current_state_index: 0, 		// index of the current state
			edge_settings_open: false, 	// whether the edge settings dialogue is open
			edit_mode: edit_mode, 			// whether the app is in edit mode
			files: [], 									// list of files that can be loaded
			icons_open: false, 					// whether the icon selection window is open
			icons: [], 									// list of all available icons
			layers_applied: {"connected": false},
			layout: initial_layout(), 	// the currently loaded layout, contains states
			node_settings_open: false, 	// whether the node settings dialogue is open
			open_open: false, 					// whether the load	window is open
			routers: [], 								// list of physical nodes
			save_open: false, 					// whether the save window is open
			selected_edge_ids: [], 			// list of selected edges
			selected_node_ids: [], 			// list of selected nodes
			sidebar_open: false, 				// whether the state selector is open
		};

		this.get_current_state = this.get_current_state.bind(this);
	}

	/**
	 * Update merge this.state.layout with a passed object
	 */
	modify_layout(layout_changes){
		let new_layout = $.extend(this.state.layout, layout_changes);
		this.setState({"layout": new_layout});
	}

	set_background(new_background) {
		let state = this.get_current_state();
		state.background = new_background;
		this.setState(state);
	}

	set_state_index(new_state_index) {
		if (new_state_index < 0) {
			return false;
		} else if (new_state_index > this.state.layout.states.length - 1) {
			return false;
		}

		// Save state before changing to new one
		this.save_visualisation_state();
		this.setState({"current_state_index": new_state_index});
		return true;
	}

	append_new_state(){
		console.log("Appending new state");
		app.state.layout.states.push({
			nodes: [],
			edges: [],
		});

		app.forceUpdate();
	}

	/**
	 * Reads the curent state from visualisation and saves the information to app.layout.state
	 * Must be called before a layout is saved or the state is changed, otherwise information will be lost
	 */
	save_visualisation_state(){
		let index = this.state.current_state_index;

		let nodes_positions = visualisation.network.getPositions();

		if(index > (this.state.layout.states.length - 1)){
			console.error("Tried to load non existent state\nindex=", index, "\nstates=", this.state.layout.states);
			return;
		}

		this.state.layout.states[index].nodes = visualisation.nodes.get().map((node) => {
			// save node position, this stops the jitter on changing state
			let pos = nodes_positions[node.id];
			node.x = pos.x;
			node.y = pos.y;
			return node;
		});

		this.state.layout.states[index].edges = visualisation.edges.get();
	}

	/**
	 * Return current state of the visualisation
	 * @return {object} Current state
	 */
	get_current_state(){
		let index = this.state.current_state_index; // int
		let state = this.state.layout.states[index]; // object

		if(state === undefined){
			console.error("Error getting state, is undefined");
		}

		return state;
	}

	edit_current_state(key, value){
		let old_layout = app.state.layout;
		let index = app.state.current_state_index;

		old_layout.states[index][key] = value;
		app.setState(old_layout);
	}

	render() {
		return (
			<div>
				<BackgroundModal set_background={this.set_background} backgrounds={this.state.backgrounds} current_background={this.get_current_state().background} open={this.state.background_open}/>
				<EdgeSettings selected_edges={this.state.selected_edge_ids} open={this.state.edge_settings_open} bearers={this.state.layout.bearers}/>
				<IconModal icons_open={this.state.icons_open} icons={this.state.icons} selected_node_ids={this.state.selected_node_ids}/>
				<Navigation edit_mode={this.state.edit_mode}/>
				<NodeSettingsWindow selected_nodes={this.state.selected_node_ids} open={this.state.node_settings_open} routers={this.state.routers}/>
				<OpenModal open={this.state.open_open} files={this.state.files}/>
				<SaveModal open={this.state.save_open} initial_name={this.state.layout.name}/>
				<StateSelector current_state_index={this.state.current_state_index} states={this.state.layout.states} open={this.state.sidebar_open}/>
				<Bearers open={this.state.bearers_open} bearers={this.state.layout.bearers}/>
				<BearerEditorWindow open={this.state.bearer_editor_open} bearers={this.state.layout.bearers}/>
			</div>
		);
	}
}

let app = ReactDOM.render(
	<App/>, document.getElementById("app"));
