extern crate rustc_serialize;
extern crate rand;
extern crate regex;

mod socket;
mod database;
mod snmp;
mod network;

use std::sync::{Arc, RwLock};
use database::{DataBase, Node};

use std::thread;
use std::time::Duration;

fn main() {
    let listener = socket::create_and_connect_socket();

    println!("Listening on {}", listener.local_addr().unwrap());
    let database = Arc::new(RwLock::new(DataBase::default()));

    {
        let copy = database.clone();
        thread::spawn(move || {
            network::find_peers(copy);
        });
        println!("Finding peers");
    }

    {
        let copy = database.clone();
        thread::spawn(move || {
            loop {
                network::update_from_network(&copy);
                thread::sleep(Duration::from_millis(500));
            }
        });
        println!("Getting peer info");
    }

    // {
    //     let copy = database.clone();
    //     thread::spawn(move || {
    //         update_generator(copy);
    //     });
    // }

    socket::listen(listener, database.clone());
}

// kept for demos without connection
// #[allow(dead_code)]
// fn update_generator(database: Arc<RwLock<DataBase>>){
//     use rand::distributions::{Range, IndependentSample};
//     let id_range = Range::new(0, 3);
//     let mut rng = rand::thread_rng();
//
//     loop{
//         {
//             let mut database = database.write().unwrap();
//             database.update_node(Node{
//                 id: id_range.ind_sample(&mut rng).to_string(),
//                 address: "".into(),
//             });
//         }
//
//         thread::sleep(Duration::from_millis(1000));
//     }
// }
