use std::process::Command;
use regex::Regex;
use database::Node;

pub fn is_up(ip: &str) -> bool {
    let command = Command::new("snmpget")
        .arg("-v1")
        .arg("-cread_only_user")
        .arg(ip)
        .arg(".1.3.6.1.2.1.1.5.0")
        .arg("-t0.5") // short timeout
        .arg("-r0") // no retries
        .output();

    let stdout: Vec<u8> = match command {
        Ok(output) => output.stdout,
        Err(e) => {
            println!("{:?}", e);
            return false;
        }
    };

    return stdout.len() > 0;
}

pub fn get(ip: &str, oid: &str) -> Option<ResponseTypes> {
    let command = Command::new("snmpget")
        .arg("-v1")
        .arg("-cread_only_user")
        .arg(ip)
        .arg(oid)
        .output();

    let stdout: Vec<u8> = match command {
        Ok(output) => output.stdout,
        Err(e) => {
            println!("{:?}", e);
            return None;
        }
    };

    return parse_snmp_response(&String::from_utf8_lossy(stdout.as_slice()).into_owned());
}

pub fn walk(ip: &str, oid: &str) {
    let command = Command::new("snmpwalk")
        .arg("-v1")
        .arg("-cread_only_user")
        .arg(ip)
        .arg(oid)
        .output();

    let stdout: Vec<u8> = match command {
        Ok(output) => output.stdout,
        Err(e) => {
            println!("{:?}", e);
            return;
        }
    };

    println!("{}", String::from_utf8_lossy(stdout.as_slice()));
}

#[derive(Debug, PartialEq)]
pub enum ResponseTypes {
    String(String),
    Integer(u32),
}


/// Parses the response from an snmp MIB
///
/// Returns None if the value was not parsable
pub fn parse_snmp_response(response: &str) -> Option<ResponseTypes> {
    let regex = Regex::new(r".* = (\w+): ?(.*)").unwrap();
    let groups = match regex.captures(response) {
        Some(groups) => groups,
        None => {
            println!("Failed to parse response: {}", response);
            println!("No groups found");
            return None;
        }
    };

    if groups.len() != 3 {
        return None;
    }


    let value_type = match groups.at(1) {
        Some(value_type) => value_type,
        None => {
            return {
                println!("Failed to find type for response {}", response);
                return None;
            }
        }
    };

    let value = match groups.at(2) {
        Some(value) => value,
        None => return None,
    };

    if value_type == "STRING" {
        return Some(ResponseTypes::String(String::from(value)));
    } else if value_type == "INTEGER" || value_type == "Gauge32" {
        return Some(ResponseTypes::Integer(value.parse::<u32>().unwrap()));
    }
    return None;
}

#[test]
pub fn test_parse_snmp_response() {
    let value = parse_snmp_response("SNMPv2-MIB::sysName.0 = STRING: test");
    assert!(value != None);
    assert_eq!(value, Some(ResponseTypes::String(String::from("test"))));

    let value = parse_snmp_response(".iso.org.dod.internet.mgmt.mib-2.interfaces.ifTable.ifEntry.\
                                     ifSpeed.2 = Gauge32: 1000");
    assert!(value != None);
    assert_eq!(value, Some(ResponseTypes::Integer(1000)));

    let value = parse_snmp_response(".iso.org.dod.internet.mgmt.mib-2.interfaces.ifTable.ifEntry.\
                                     ifPhysAddress.2 = STRING: b8:27:eb:ba:af:5");
    assert!(value != None);
    assert_eq!(value,
               Some(ResponseTypes::String(String::from("b8:27:eb:ba:af:5"))));

    let value = parse_snmp_response(".iso.org.dod.internet.mgmt.mib-2.interfaces.ifTable.ifEntry.\
                                     ifPhysAddress.1 = STRING:");
    assert!(value != None);
    assert_eq!(value, Some(ResponseTypes::String(String::from(""))));
}


/// Fill in a node information
pub fn fill_node(ip: &str) -> Node {
    let id = match get(ip, ".1.3.6.1.2.1.1.5.0") {
        Some(ResponseTypes::String(id)) => id,
        _ => panic!("Id is not a string"),
    };

    let load: f32 = match get(ip, ".1.3.6.1.4.1.2021.10.1.3.1") {
        Some(ResponseTypes::String(load)) => {
            match load.parse::<f32>() {
                Ok(load) => load,
                Err(e) => {
                    println!("Failed to parse {:?} into a f32", load);
                    println!("{:?}", e);
                    0.0
                }
            }
        }
        _ => panic!("Load is not a string"),
    };

    Node {
        id: id,
        load: load,
        address: String::from(ip),
    }
}
