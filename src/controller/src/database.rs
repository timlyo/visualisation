use rustc_serialize::json;
use std::collections::BTreeMap;

#[derive(Debug, Default)]
pub struct DataBase {
    pub nodes: Vec<Node>,
    updates: Vec<Update>,
}

impl DataBase {
    /// Make changes to a node and adds the diffs to the updates struct to be sent to the display
    /// Adds node if it doesn't exist
    ///
    /// Sorts node list if changes are made
    pub fn update_node(&mut self, new_node: Node) {
        match self.nodes.iter().position(|node| node.id == new_node.id) {
            Some(position) => {
                let update = self.nodes[position].diff_as_update(&new_node);
                if update.is_some() {
                    println!("{:?}", update);
                    self.updates.push(update.unwrap());
                }
                self.nodes[position] = new_node;
            }
            None => {
                self.updates.push(new_node.as_update());
                println!("Added new node: {} {}", &new_node.id, &new_node.address);
                self.nodes.push(new_node);
            }
        }
    }

    /// Return the contents of DataBase::updates and clear the vector
    pub fn get_updates(&mut self) -> Vec<Update> {
        let updates = self.updates.clone();
        self.updates.clear();
        return updates;
    }
}

#[derive(Debug, RustcEncodable, Clone)]
pub struct Node {
    pub id: String,
    // TODO turn type into net::IpAddr. Currently a string
    // so that json::RustcEncodable doesn't need to be implemented
    pub address: String,
    pub load: f32,
}

#[test]
fn test_diff_node() {
    use std::net::Ipv4Addr;

    let node1 = Node {
        id: "1".into(),
        address: "192.168.1.1".into(),
        load: 1.0,
    };

    let node2 = Node {
        id: "1".into(),
        address: "192.168.1.1".into(),
        load: 2.0,
    };

    let diff = node1.diff(&node1);
    assert!(diff.is_none());

    let diff = node1.diff(&node2);
    assert!(diff.is_some());
    // TODO check correct key is found
}

impl Node {
    pub fn diff(&self, node: &Node) -> Option<String> {
        let mut diffs = BTreeMap::new();
        let mut changed = false;

        // different id is probably an error
        if self.id != node.id {
            diffs.insert("id", node.id.clone());
            changed = true;
        }

        if self.address != node.address {
            diffs.insert("address", node.address.clone());
            changed = true;
        }

        if self.load != node.load {
            diffs.insert("load", node.load.clone().to_string());
            changed = true;
        }

        if !changed {
            return None;
        }

        return Some(json::encode(&diffs).unwrap());
    }

    pub fn diff_as_update(&self, node: &Node) -> Option<Update> {
        let diff = self.diff(node);
        if diff.is_none() {
            return None;
        }

        return Some(Update {
            id: self.id.clone(),
            changes: diff.unwrap(),
        });
    }

    pub fn as_update(&self) -> Update {
        let changes = match json::encode(self) {
            Ok(changes) => changes,
            Err(e) => {
                println!("Error encoding {:?}", self);
                println!("{:?}", e);
                panic!(e);
            }
        };

        return Update {
            id: self.id.clone(),
            changes: changes,
        };
    }
}

/// Type designed to be sent to the display with changes encoded in a json string
#[derive(Debug, RustcEncodable, Clone)]
pub struct Update {
    pub id: String,
    pub changes: String,
}
