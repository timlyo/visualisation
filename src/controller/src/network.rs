use std::net;
use std::process;
use std::str::FromStr;
use std::sync::{Arc, RwLock};

use regex::Regex;

use snmp;
use database::DataBase;

/// everything for communicating with the physical nodes

// TODO peer cache
// Peers are likely to have the same ip address between sessions, so these could be cached and
// immediatley checked on startup

/// Scan local network and check if any are running snmp with an
/// accepted community string
pub fn find_peers(database: Arc<RwLock<DataBase>>) {
    let peers = match from_nmap(){
        Some(peers) => peers,
        None => Vec::new()
    };

    for peer in peers {
        let ip = &format!("{}", peer);
        if snmp::is_up(ip){
            println!("Found peer: {:?}", peer);
            {
                let mut database = database.write().unwrap();
                database.update_node(snmp::fill_node(ip))
            }
        }
    }
}

/// Run nmap and parse the output to check for possible peers
pub fn from_nmap() -> Option<Vec<net::IpAddr>> {
    let command = process::Command::new("nmap")
        .arg("172.10.10.0/24") // TODO get ip
        .arg("-sn") // ping scan only
        .args(&["--max-retries", "0"])
        .arg("-T4") // aggressive scan speed
        .output();

    let output: Vec<u8> = match command {
        Ok(output) => output.stdout,
        Err(e) => {
            println!("Error getting nmap output {:?}", e);
            return None;
        }
    };

    Some(
        String::from_utf8_lossy(&output)
        .lines()
        .filter_map(|line|{
            get_ip_from_nmap_line(&line)
        })
        .collect()
    )
}

#[test]
fn test_get_ip_from_nmap_line() {
    assert!(get_ip_from_nmap_line("Nmap scan report for 172.10.10.1") == Some(net::IpAddr::from_str("172.10.10.1").unwrap()));
    assert!(get_ip_from_nmap_line("Nmap scan report for 172.10.10.255") == Some(net::IpAddr::from_str("172.10.10.255").unwrap()));
    assert!(get_ip_from_nmap_line("Nmap scan report for 172.10.10.300") == None);
    assert!(get_ip_from_nmap_line("Nmap scan report for 172.10.10") == None);
    assert!(get_ip_from_nmap_line("ajsgjdagfaufahuf") == None);
}

pub fn get_ip_from_nmap_line(line: &str) -> Option<net::IpAddr>{
    if line.starts_with("Nmap scan report for") == false {
        return None;
    }

    // TODO make regex only compile once
    // see lazy static crate
    let regex_string = "(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$";
    let ip_regex = Regex::new(regex_string).unwrap();

    let captures = match ip_regex.captures(line){
        Some(ip) => ip,
        None => return None
    };

    match captures.at(0) {
        // shouldn't fail due to regex check earlier ... probably
        Some(ip) => Some(net::IpAddr::from_str(ip).unwrap()),
        None => None
    }
}


/// Iterate through all nodes in the database and get updates from the physical routers
/// Updates are pushed to the database
pub fn update_from_network(database: &Arc<RwLock<DataBase>>){
    let mut database = database.write().unwrap();

    for node in database.nodes.clone().iter() {
        database.update_node(snmp::fill_node(&node.address));
    }
}
