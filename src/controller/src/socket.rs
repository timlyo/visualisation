/// Everything for communicating with the display


use std::net;
use std::net::{TcpListener, TcpStream};
use std::process;
use std::io::{Read, Write};
use std::thread;
use std::sync::{Arc, RwLock};

use std::str;

use rustc_serialize::{json, Encodable};
use database::DataBase;


#[derive(RustcDecodable, Debug)]
struct Command {
    action: String,
}

pub fn create_and_connect_socket() -> TcpListener {
    match TcpListener::bind("127.0.0.1:5001") {
        Ok(listener) => return listener,
        Err(e) => {
            println!("{:?}", e);
            process::exit(-1);
        }
    };
}

pub fn listen(socket: TcpListener, database: Arc<RwLock<DataBase>>) {
    for stream in socket.incoming() {
        match stream {
            Ok(stream) => {
                let database = database.clone();
                thread::spawn(move || {
                    println!("Stream Connected");
                    handle_stream(stream, database);
                    println!("Dropped stream");
                });
            }
            Err(e) => println!("{:?}", e),
        }
    }
}

fn handle_stream(mut stream: TcpStream, database: Arc<RwLock<DataBase>>) {
    loop {
        let data = match read_stream(&mut stream) {
            Ok(data) => data,
            Err(e) => {
                println!("{:?}", e);
                return;
            }
        };

        let data: Command = match json::decode(&data) {
            Ok(data) => data,
            Err(e) => {
                println!("{:?}", e);
                println!("{:?}", &data);
                continue;
            }
        };

        match &data.action as &str {
            "get_updates" => send_database_updates(&mut stream, &database),
            _ => {}
        }
    }
}

fn send_database_updates(mut stream: &mut TcpStream, database: &Arc<RwLock<DataBase>>) {
    let mut database = database.write().unwrap();
    let updates = database.get_updates();
    send_message(updates, &mut stream)
}

fn read_stream(stream: &mut TcpStream) -> Result<String, String> {
    let mut buf = [0; 1024];
    let size: usize = match stream.read(&mut buf) {
        Ok(size) => size,
        Err(e) => {
            return Err(format!("Error reading stream {:?}", e));
        }
    };

    if size == 0 {
        let _ = stream.shutdown(net::Shutdown::Both);
        return Err("Stream shutdown".into());
    }

    let string_buf: String = match str::from_utf8(&buf[..size]) {
        Ok(string_buf) => String::from(string_buf),
        Err(e) => {
            return Err(format!("Error reading stream bytes {:?}", e));
        }
    };

    return Ok(string_buf);
}

fn send_message<T: Encodable>(message: T, stream: &mut TcpStream) {
    let encoded: String = match json::encode(&message) {
        Ok(encoded) => encoded,
        Err(e) => {
            println!("Failed to encode json response {:?}", e);
            return;
        }
    };

    let size = encoded.len();
    if size > 99999 {
        panic!("Message is longer than 99999 characters, not supported by protocol");
    }

    let message = format!("{:5}{}", size, encoded);
    let _ = stream.write_all(message.as_bytes());
}
