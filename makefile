dir = dist/visualisation

all:\
	display\
	dist/install.sh\
	dist/visualisation/controller\
	dist/visualisationController.service\
	dist/visualisationDisplay.service\
	files\
	libs\
	python\

doc:\
	docs/compiled/bearer.html\
	docs/compiled/controller.html\
	docs/compiled/directories.html\
	docs/compiled/display.html\
	docs/compiled/install.html\
	docs/compiled/layers.html\
	docs/compiled/layout.html\
	docs/compiled/overview.html\
	docs/compiled/structure.svg\

	cd src/controller; cargo doc
	cp -r src/controller/target/doc/ docs/compiled/controller

clean:
	rm -rf dist
	rm -rf tmp

display:\
	dist/visualisation/display/templates/index.html\
	python\
	typescript\
	css\
	react\
	dist/visualisation/display/static/js/tests.js\
	dist/visualisation/display/static/js/components.js

	gulp components

dist/visualisation/display/static/js/tests.js:\
	tmp/tests/_util.js\
	tmp/tests/test_bearer.js\
	tmp/tests/test_complete_node.js\
	tmp/tests/test_complete_style.js\
	tmp/tests/test_layout.js\
	tmp/tests/test_node_settings.js\
	tmp/tests/test_state.js\
	tmp/tests/test_test.js\
	tmp/tests/test_util.js\
	tmp/tests/test_visualisation.js\
	tmp/tests/test_windows.js\

	cat tmp/tests/*.js >> dist/visualisation/display/static/js/tests.js

libs:
	gulp move_libs

js_dir := dist/visualisation/display/static/js
typescript:\
	$(js_dir)/bearers.js\
	$(js_dir)/data.js\
	$(js_dir)/edges.js\
	$(js_dir)/init.js\
	$(js_dir)/interface.js\
	$(js_dir)/layer.js\
	$(js_dir)/nodes.js\
	$(js_dir)/shortcuts.js\
	$(js_dir)/socket.js\
	$(js_dir)/system.js\
	$(js_dir)/util.js\
	$(js_dir)/visualisation.js

react:\
	dist/visualisation/display/static/js/app.js

css: $(dir)/display/static/css/style.css

python:\
	$(dir)/display/__init__.py\
	$(dir)/display/data.py\
	$(dir)/display/msg.py\
	$(dir)/display/routes.py\
	$(dir)/main.py\
	$(dir)/venv/

files:\
	dist/files/default.png

dist/visualisation/display/static/js/app.js: src/display/app.jsx
	@mkdir -p dist/visualisation/display/static/js
	babel $< -o $@ --presets react,es2015

dist/visualisation/display/static/js/components.js:\
	tmp/components/backgroundSelector.js\
	tmp/components/bearerEditorControls.js\
	tmp/components/bearerEditorList.js\
	tmp/components/bearerEditorWindow.js\
	tmp/components/bearersWindow.js\
	tmp/components/dropzone.js\
	tmp/components/edgeSettings.js\
	tmp/components/iconItem.js\
	tmp/components/iconSelector.js\
	tmp/components/imageGrid.js\
	tmp/components/navigation.js\
	tmp/components/nodeSettingsForm.js\
	tmp/components/nodeSettingsWindow.js\
	tmp/components/open.js\
	tmp/components/save.js\
	tmp/components/stateSelector.js\
	tmp/components/titleBar.js\

	cat tmp/components/*.js >> dist/visualisation/display/static/js/components.js

dist/visualisation/venv/:
	@mkdir -p dist/venv
	rsync -aup venv dist

dist/visualisation/main.py: src/main.py
	@mkdir -p dist/visualisation/
	cp src/main.py dist/visualisation/main.py

dist/visualisation/controller:
	cargo build --release --manifest-path src/controller/Cargo.toml
	@mkdir -p dist/visualisation
	cp src/controller/target/release/controller dist/visualisation/

dist/install.sh: configs/install.sh
	cp configs/install.sh dist/install.sh

dist/visualisationDisplay.service: configs/visualisationDisplay.service
	cp configs/visualisationDisplay.service dist/visualisationDisplay.service

dist/visualisationController.service: configs/visualisationController.service
	cp configs/visualisationController.service dist/visualisationController.service

dist/visualisation/display/static/css/style.css: src/display/sass/style.sass
	@mkdir -p dist/visualisation/display/static/css
	sass $< $@

## Patterns
dist/files/%: files/%
	@mkdir -p dist/files
	cp $< $@

dist/visualisation/display/%.py: src/display/%.py
	@mkdir -p dist/visualisation/display
	cp $< $@

dist/visualisation/display/templates/index.html: src/display/templates/index.html
	@mkdir -p dist/visualisation/display/templates
	cp $< $@

dist/visualisation/display/static/js/%.js: src/display/js/%.ts
	-tsc $< --outFile $@

tmp/tests/%.js: src/display/tests/%.ts
	-tsc $< -outFile $@

tmp/components/%.js: src/display/components/%.jsx
	@mkdir -p tmp/components
	babel $< -o $@ --presets react,es2015

docs/compiled/%.html: docs/%.md
	@mkdir -p docs/compiled
	pandoc $< -o $@

docs/compiled/%.svg: docs/%.dot
	@mkdir -p docs/compiled
	dot $< -Tsvg -o $@
