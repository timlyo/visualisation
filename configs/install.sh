#!/usr/bin/bash

if [ "$(id -u)" != "0" ]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi

display_running=false
if [ `systemctl is-active visualisationDisplay` == "active" ]; then
  echo "Stopping display service"
  display_running=true
  systemctl stop visualisationDisplay
fi

controller_running=false
if [ `systemctl is-active visualisationController` == "active" ]; then
  echo "Stopping controller service"
  controller_running=true
  systemctl stop visualisationController
fi

echo "Making visualisation account"
useradd -r visualisation

echo "Installing visualisation files"
mkdir -p /usr/share/visualisation
cp -r visualisation/* /usr/share/visualisation

echo "Installing virtual environment"
cp -r venv /usr/share/visualisation/

echo "Installing service files"
cp visualisation*.service /usr/lib/systemd/system/
systemctl daemon-reload

echo "Making data directory"
mkdir -p /var/visualisation/backgrounds
mkdir -p /var/visualisation/saves/deleted/
mkdir -p /var/visualisation/icons
chown -R visualisation:visualisation /var/visualisation

echo "Copying static files"
cp files/default.png /var/visualisation/backgrounds/

if [ display_running ]; then
  echo "Restarting display"
  systemctl start visualisationDisplay
fi

if [ controller_runningz ]; then
  echo "Restarting controller"
  systemctl start visualisationController
fi
