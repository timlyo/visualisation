module.exports = {
	"env": {
		"browser": true,
		"es6": true,
		"jquery": true
	},
	"extends": "eslint:recommended",
	"parserOptions": {
		"sourceType": "module",
		"ecmaFeatures": {
			"experimentalObjectRestSpread": true,
			"jsx": true
		}
	},
	"plugins": [
		"react"
	],
	"rules": {
		"indent": [
			2,
			"tab"
		],
		"linebreak-style": [
			2,
			"unix"
		],
		"quotes": [
			2,
			"double"
		],
		"semi": [
			2,
			"always"
		],
		"no-unused-vars": 0,
		// "no-undef": 0,
		"no-console": 0,
		"comma-dangle": 0,
		"indent":[
			2,
			"tab",
			{SwitchCase: 1}
		],
	}
};
