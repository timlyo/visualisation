/* globals require */

var babel = require("gulp-babel");
var concat = require("gulp-concat");
var gulp = require("gulp");
var gutil = require("gulp-util");
var uglify = require("gulp-uglify");

gulp.task("default", ["move_libs", "app", "components"]);

gulp.task("components", function() {
	return gulp.src("src/display/components/*.jsx")
		.pipe(babel({
			presets: ["es2015", "react"],
		}))
		.on("error", (info) => {
			gutil.log(info);
		})
		.pipe(concat("components.js"))
		.pipe(gulp.dest("dist/visualisation/display/static/js"));
});

gulp.task("move_libs", function() {
	gulp.src([
		"lib/jquery/jquery-1.12.1.js",
		"lib/cookie/js.cookie-2.1.0.min.js",
		"lib/dropzone/dropzone.js", // must be after jquery
		"lib/interact/interact.js",
		"lib/mousetrap/mousetrap.js",
		"lib/notify/notify.js",
		"lib/react/react.js", // must be before react-dom
		"lib/react/react-dom.js",
		"lib/semantic-ui/semantic.js",
		"lib/socketio/socket.io-1.4.5.js",
		"lib/vis/vis.js",
	])
		.pipe(uglify())
		.pipe(concat("lib.js"))
		.pipe(gulp.dest("dist/visualisation/display/static/js"));

	// CSS
	gulp.src([
		"lib/dropzone/dropzone.css",
		"lib/dropzone/basic.css",
		"lib/semantic-ui/semantic.css",
		"lib/vis/vis.css",
	])
		// .pipe(clean_css())
		.pipe(concat("bundle.css"))
		.pipe(gulp.dest("dist/visualisation/display/static/css"));

	gulp.src("lib/semantic-ui/themes/**")
		.pipe(gulp.dest("dist/visualisation/display/static/css/themes"));
});
