The display is responsible for displaying data, taking user input and maintaining a copy of the state that is sent to it from the controller.

# Network Rendering

vis.js is a flexible graph layout and rendering engine containing the required features.

semantic ui is a flexible layout framework for web applications.

# Communication

# Interface

This is the visual aspect of the display.

# Layouts

The display is responsible for managing the saved layouts. They will then be saved as a json document in ~/.visualisation on Linux or the windows equivalent.

This is discussed in more detail in [layout](./layout.md).


# API

The display must expose an ajax api to the interface.

| Method   | Route                  | Description                        |
|:---------|:-----------------------|:-----------------------------------|
| `GET`    | `/api/icon`            | Return a list of icons             |
| `GET`    | `/api/icon/<id>`       | Return an icon image               |
| `POST`   | `/api/icon`            | Upload a new icon                  |
| `GET`    | `/api/background`      | Return a list of backgrounds       |
| `GET`    | `/api/background/<id>` | Return a background image          |
| `POST`   | `/api/background`      | Upload a new background            |
| `GET`    | `/api/styles`          | Return a list of edge styles       |
| `POST`   | `/api/styles`          | Upload a new style                 |
| `GET`    | `/api/load`            | Return a list of the saved layouts |
| `GET`    | `/api/load/<id>`       | Return a single saved layout       |
| `POST`   | `/api/save/<id>`       | Save data as a document            |
| `DELETE` | `/api/load/<id>`       | Delete a layout                    |

# Process

This section documents the process that the display will go through during various states

## Open layout

The system will load a list of files and some basic details, such as name, date, and description. These will then be stored in `app.state.files`.

The user will specify a layout to open from a list compiled from app.state.files and displayed in the open dialogue. The layout will be specified by it's name. The client will send a request to the server for the json document describing the layout, which will then be saved in `app.state.layout`. The contents of the layout is discussed in more details in [layout](./layout.md)

## Save layout

A name for the layout is collected and then the current layout is sent to the server in a POST request. The server will then create a file for the layout if it doesn't exist, or overwrite the current file if it does. The display should check that the user wants to overwrite the file before doing so.
