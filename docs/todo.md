# Current
* Modal scrollbar - add a scrollbar to open and save modals if there is too much content to display
* Reload save list on save
* Deleting saves
* Deleting backgrounds
* Deleting icons
* Layers
* copy background from previous state if undefined rather than using default
* netflow snmp extensions

# Long Term
* add description box to save window
* settings window
* Preload backgrounds so they aren't loading during presentation
* warning for conflicting bearer names
* overlay text - Text that appears on every state, such as the title or section name
* controller update queue squashing - e.g. [{prop: 1}, {prop: 2}] becomes [{prop: 2}] to reduce number of updates
* Remove node's image and set it back to the defualt style after adding a node
* unsaved changes indicator
* Node shadow settings
* Change snmp in rust too ffi library calls
* PKGBUILD check
* Release build
  * rust --release flag
  * minified libraries and code

# Nice to have
* Warning about some node settings only working with icons
* watch inodes of backgrounds/icons and send updates on changes
* Convert backgrounds to webp format
* smaller thumbnails for background images
* add option to sort bearer list
  * alphabetically
  * Most common
* add filter for bearer list
  * currently in state
  * currently used in layout
* fix bearer window alignment for long bearer names
* layout style editing
* Editing states
  * add to middle of list
  * reorganise states
* Scroll sidebar down as cursor moves
* add option to only show bearers that are active on the display
* better handling of empty cases
  * handle empty bearer list in bearer editor
* add bearer preview to editor window
* Importing information from other layouts for ease of creation
  * Bearer styles
