The controller is responsible for communicating with routers and maintaining the information that is gathered to be passed to the visualisation. It can be broken down into multiple domains.

# Communication

The controller will maintain a socket on port 5001 that can be queried by sending `{action: "get_updates"}` encoded within json, handled in `src/display/msg.py`

The controller will store the previous state of the network, and upon detecting a change, will update it's internal database.

It will monitor all information on the network. This includes

* Nodes coming online
* Connections between nodes being removed
* Node information changing

Connections between nodes is considered different to node information because it is pertinent to multiple nodes.

# Messages

The controller should respond to a series of messages

## Actions

`{action: "action_name"}`

### get_updates
