This document explains the directory structure of the project, and what each directory contains

* **Root** - Contains base files such as readme, build scripts, and configs
  * **configs** - Contains configs that will be included in the final package
  * **dist** - The final runtime, only created after the project is built
  * **files** - Static files included in the final package, e.g. images
  * **node_modules** - Container for js bloat
  * **src** - All source files
    * **controller** - Controller source files, standard rust layout
    * **display** - Display source files
      * **components** - React components
      * **js** - typescript files, compiles to js
      * **sass** - sass files, compiles to css
      * **templates** - contains index.html, standard Flask structure
      * **tests** - typescript tests
  * **venv** - python virtual environment
* **/var/visualisation/** - visualisation files, uploaded and saved files go here
  * **backgrounds** - Saved background images
  * **icons** - Saved icon images
  * **saves** - saved layouts
    * **deleted** - deleted layouts are moved here to give the opportunity to recover them
