<!-- TOC depthFrom:1 depthTo:6 withLinks:1 updateOnSave:1 orderedList:0 -->

- [Introduction](#introduction)
	- [Purpose](#purpose)
	- [Scope](#scope)
	- [Definitions](#definitions)
- [Design](#design)
	- [Display](#display)
	- [Controller](#controller)
	- [Router](#router)
	- [Network representation](#network-representation)
	- [Commands](#commands)
	- [Command processing](#command-processing)
	- [Response processing](#response-processing)

<!-- /TOC -->

# Introduction

## Purpose

This document is intended to discuss the design of the visualisation software.

## Scope

This document talks about the system design and architecture of the Visualisation project.

## Definitions

| Term       | Definition                                                                                    |
|:-----------|:----------------------------------------------------------------------------------------------|
| Controller | Component responsible for communicating with the routers and notifying the display of updates |
| Display    | The visual component that the user interacts with                                             |
| Edge       | A connection between two nodes on the diagram                                                 |
| Network    | The connected computers that are being visualised                                             |
| Node       | Data point in the virtual network                                                             |
| Operator   | Person controlling the visualisation                                                          |
| Router     | Physical machine in the network                                                               |
| Viewer     | Person viewing the display                                                                    |

# Components
The system has been broken down into 3 main parts.

* Display
* Controller
* Router

These components are discussed in more detail in their individual documents.

## Display

This part of the system is responsible for representing the data.

The Display is being implemented as a web app with a python backend. This means that it is portable, the only requirement is a modern browser and a python 3 run time; and it can leverage modern web technology.

The display is not responsible for managing or otherwise editing any non visual aspects of the network.

## Controller

This component is responsible for getting the information from the routers, formatting this information, and sending updates to the display.


## Router

This component is the physical aspect of the network.

# Data Representation

One of the key aspects of the design is how the network is represented as a data structure.

## Network representation

One of the key aspects of the design is how the network is represented as a data structure.

Each node in the network must have a unique id. This will be used during the rendering step to connect each node to their peers, and must be unique to ensure accurate topology representation.

In a single subnet environment an ip or mac address would be suitable. In a multiple subnet environment each node would have multiple interfaces, and thus multiple ips and mac addresses and possible conflicts; therefore neither would be a suitable identifier.

The id can either be provided by a central source, preset individually, randomly generated, or generated from machine specific identifiers.

Every node must also store their neighbours, and each neighbour must be identifiable. This can either store the unique id of the neighbour, or store the ip address.

The advantage of storing the id is that this can be used later to identify the node, simplifying the step of graphically representing the data. The downside is that the node would have to be contacted before this information can be used. This method would be sufficient as long as this information stays on the node that collected it and is only used to identify neighbours from by node, rather than later in the rendering.

Storing the ip would simplify the problem of individual nodes contacting their neighbours, although it would introduce another step that would include collecting the unique ids of the network for rendering.

Storing both gives the advantages of both systems, while only having the disadvantage of introducing an additional step of obtaining the unique id

A HashSet is used for storage of the neighbours to ensure uniqueness.

```
Node{
	id: String
	neighbours: HashSet[{
		ip: IpAddr,
		id: option<String>
	}]
}
```
