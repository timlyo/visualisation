# Layout
A layout is a container for everything to do with the visual aspects of a visualisation. Each layout is stored as a directory.

## State
States are stored as an array within a layout. States can be progressed in a linear fashion, similar to slides in a slideshow.

A state is a collection of nodes, edges .

The user must be able to change the current state. Therefore the index of the current state must be saved in the store.

The information of the state must be saved in two places. Firstly the layout in the store, and secondly within the visualisation in the form that vis.js requires. The storage for vis.js must be the most up to date because it is what the user immediately interacts with. Therefore state layout inside the store is considered a cold copy that is only updated when the layout is loaded from the server, or when the current state is changed. The layout inside the store is the one that is sent to the server when it is saved.

Each state has a background associated with it. If no background is associated then `default.png` is loaded from the server.

## Structure

```
layout.json{
    name: String
    date: String
    bearers: [
        name: String
        style: {}
    ]
    States: [
        {
            nodes: []
            edges: []
            background
        }
    ]
}
```
