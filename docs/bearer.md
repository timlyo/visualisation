A bearer is defined as a system that carries information between two nodes in a network.

The system must be able to store different types of bearer, and allow the user to choose how to differentiate between them. Available style choices are

- arrows
- colour
- dashes
- label
- shadow
- width

Bearers will be defined per layout, and each state will define each edge with a bearer type. The edge will then be styled depending on the preset rules.

There will be an option to import bearers from another saved layout to save time *eventually*.

http://visjs.org/docs/network/edges.html

```
Bearer{
  name: string,
  dashes: bool,
  color: object or string
}
```
