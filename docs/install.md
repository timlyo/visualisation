The system was designed around Arch Linux, but should work transparently on any distribution running systemd.

# Building
## Make

The overarching task runner. Running make in the root directory will build all files and copy them into dist/

This can be easily done using a inotify file watcher such as https://rerun.github.io/rerun/

## Gulp

Used to build and package the JavaScript and css portions of the project. This is called within the makefile and doesn't need to be manually called during the build process.

## Cargo

Used to compile rust code. This is called by gulp and the resulting executable is stored inside of dist. It does not need to be called manually, but can be called to only build/run that section.

## Requirements

### Building
* JavaScript
  * gulp
    * gulp-babel
    * gulp-clean-css
    * gulp-typescript
    * gulp-uglify
    * gulp-sass
    * gulp-util
    * gulp-watch
* GNU make
* Rust nightly and Cargo

### Running
* Python 3 - documented in requirements.txt
* glibc
* nmap - used by controller
* snmp - used by controller

# Install

Copy the dist directory (created during build) to the machine that the visualisation will run on and run `install.sh` with root privileges.

The script will create a visualisation account and install the files as follows:

* dist/files/default.png -> /var/visualisation/backgrounds/
* dist/visualisation/ -> /usr/share/visualisation/
* dist/venv/ -> /usr/share/visualisation/
* dist/\*service -> /usr/lib/systemd/system/

It also creates `/var/visualisation/` for saved files and uploaded images.

If the services are running before the install then it will be stopped and restarted after. Otherwise the service must be started via `systemctl start visualisationDisplay|visualisationController`
