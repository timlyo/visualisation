A layer is a set of modifications that can be drawn over the top of an existing network diagram to provide additional information.

# Output

Each layer can manipulate the following set of properties within the visualisation:

- edge properties
  - arrows
  - colour
  - dashes
  - hidden
  - label
  - length
  - shadow
  - width
- node properties
  - colour *
  - hover text
  - label
  - scale
  - shadow
  - visibility (true|false)

\* wont show up when using an icon for the node

Alongside drawing on top of the visualisation in the `afterDrawing` visualisation callback (example  in visualisation.ts)

# Input

The information is collecteed by the controller, which is then polled by the server in `src/display/msg.py` and forwarded to the visualisation over a websocket. 
